#include <iostream>

template <typename C, typename V>
void print_result(C comment, V value)
{
  std::cout << comment << value << std::endl;
}

int foo(int *ptr) { return *ptr; }

void testPtrs4_1()
{
  // https://metanit.com/cpp/tutorial/4.1.php
  int num = 5;
  int *pointer = &num;

  print_result("variable num address: ", pointer);
  print_result("address of num: ", &num);
  print_result("address of pointer: ", &pointer);
  print_result("variable num value by pointer: ", *pointer);
  print_result("variable num value by name: ", num);
}

void testPtrs4_2_1()
{

  // // https://metanit.com/cpp/tutorial/4.2.php

  int num2 = 10;
  int *pointer = &num2;
  *pointer = 10;
  std::cout << "*pointer = 10;" << std::endl;

  pointer = &num2;
  // t buff[10];
  // pointer = buff;

  print_result("variable num address: ", pointer);
  print_result("variable num value by pointer: ", *pointer);
  print_result("variable num value by name: ", num2);

  ++pointer;
  /*
  address: 0x7fffffffddcc
  pointer address: 0x7fffffffddc0
  value: 5
  from pointer after change: 10
  num value  after change: 10
  */
}

void testPtrs4_2_2()
{
  int a{10};
  int b{2};

  int *pointer_a{&a};
  int *pointer_b{&b};

  print_result("pointer_a address: ", pointer_a);
  print_result("value a by pointer: ", *pointer_a); // 10
  print_result("pointer_b address: ", pointer_b);
  print_result("value b by pointer: ", *pointer_b); // 2

  pointer_a = pointer_b;
  std::cout << "pointer_a = pointer_b;" << std::endl;
  print_result("pointer_a address: ", pointer_a);
  print_result("value a by pointer: ", *pointer_a); // 2

  *pointer_a = 102;
  std::cout << "*pointer_a = 102;" << std::endl;
  print_result("value b: ", b); // 102?
  int *pointer_e{};
  int *&pointerRef{pointer_e};

  pointerRef = &a;
  print_result("a in pointer_e value: ", *pointer_e); // 10
  pointerRef = &b;
  print_result("b in pointer_e value: ", *pointer_e); // 102
}


int foo(int &ref)
{
  ref = 16;
  return ref;
}

int foo(const int &ref)
{
  return 0;
}

constexpr int factorial(int x)
{
  if (x == 1)
    return 1;
  return x * factorial(x - 1);
}

static_assert(factorial(5) == 120, "");

int main()
{
 // testPtrs4_1();
  // testPtrs4_2_1();
  // testPtrs4_2_2();

  // int buff[8];
  int c = 20;
  // int d = 10;
  long p;
  // print_result("variable c value: ", c);
  int &ref_c = c;
  // std::cout << "++c" << std::endl;
  // ++ref_c;
  // print_result("value c by reference: ", ref_c);
  // ref_c = d;
  // print_result("value c by reference: ", ref_c);

  auto e = foo(ref_c);
  // const int &e_ref = e;
  // foo(e_ref);
  // foo(&ref_c);

  // int *c_ptr = &c;

  // if (c_ptr)
  //   int &c_ref_p = *c_ptr;

  struct A
  {
    int field = 0;
  };

  A a; 

  A *a_ptr = &a;
  a_ptr->field;

  A &a_ref = a;
  a_ref.field;

  int buff2[8];
  

  static_assert(sizeof(A) == sizeof(int), ""); //!

  auto g = static_cast<int>(p);
  print_result("value e from func: ", e);
}
