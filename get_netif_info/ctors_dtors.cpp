#include <iostream>
#include <memory>

struct Resource {
  Resource() { std::cout << "Resourse has been acquired\n";}
  ~Resource() { std::cout << "Resource has been released\n";}
};

struct Base {
  Base() { std::cout << "Base Constructor Called\n";}
  ~Base() { std::cout << "Base Destructor called\n";}
};

struct Derived1: Base {
  Derived1() { 
    ptr = std::make_unique<Resource>(); 
    std::cout << "Derived constructor called\n";
  }
  ~Derived1() {std::cout << "Derived destructor called\n";}
private:
  std::unique_ptr<Resource> ptr;
};

int main() {
  Base *b = new Derived1();
  delete b;
}