#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <net/if.h>


struct iplink_req {
	struct nlmsghdr         n;
	struct ifinfomsg        i;
	char                    buf[1024];
};

int main(int argc, char *argv[])
{
	const char *dev = NULL;
	struct iplink_req req;
	struct rtnl_handle rth = { .fd = -1 };
	unsigned int ifindex = 0;
	int len = 0;

	if (argc < 2) {
		fprintf(stderr, "usage: rmif <dev>\n");
		exit(1);
	}

	dev = argv[1];
	ifindex = if_nametoindex(dev);
	if (!ifindex) {
		fprintf(stderr, "Invalid device %s\n", dev);
		exit(1);
	}

	len = strlen(dev);
	if (len > IFNAMSIZ) {
		fprintf(stderr, "Interface name too long: %s\n", dev);
		exit(1);
	}

	if (rtnl_open(&rth, 0)) {
		fprintf(stderr, "Cannot open rtnetlink!\n");
		exit(1);
	}

	memset(&req, 0, sizeof(req));
	req.n.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifinfomsg));
	req.n.nlmsg_flags = NLM_F_REQUEST;
	req.n.nlmsg_type = RTM_DELLINK;
	req.i.ifi_family = AF_UNSPEC;
	req.i.ifi_index = ifindex;

	addattr_l(&req.n, sizeof(req), IFLA_IFNAME, dev, len);

	if (rtnl_talk(&rth, &req.n, 0, 0, NULL, NULL, NULL) < 0) {
		fprintf(stderr, "failed to talk to netlink!\n");
		exit(1);
	}

	rtnl_close(&rth);

	printf("Deleted interface %s\n", dev);
}
/*
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <linux/rtnetlink.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <unistd.h>
#include <iostream>
#include <string_view>
#include <cstring>
#include <list>
#include <libnetlink.h>


class IfDescr{
public:

  IfDescr(const std::string_view ifname,
          const std::string ipv4,
          const std::string brd,
          const uint8_t prefix,
          const std::string mac_addr);

  std::string ipv4() const;
  std::string ifname() const;
  std::string broasdcast() const;
  uint8_t prefix() const;

  std::string mac_aadr();

private:
  const std::string _if_name;
  const std::string _ipv4;
  const std::string _brd;
  const uint8_t _prefix;
  const std::string _mac_addr;

};

IfDescr::IfDescr(std::string_view ifname, std::string ipv4, std::string brd, const uint8_t prefix, std::string mac_addr)
    :
      _if_name(ifname),
      _ipv4(ipv4),
      _brd(brd),
      _prefix(prefix),
      _mac_addr(mac_addr)

{}

std::string IfDescr::ipv4() const{
    return _ipv4;
}

std::string IfDescr::ifname() const{
    return _if_name;
}

std::string IfDescr::broasdcast() const{
    return _brd;
}


uint8_t IfDescr::prefix() const
{
    return _prefix;
}


std::string IfDescr::mac_aadr(){
    return _mac_addr;
}


class InfoCollertor
{
public:
    InfoCollertor(const InfoCollertor &) = delete;
    InfoCollertor &operator=(const InfoCollertor &) = delete;
    InfoCollertor()
    {
        // Create a socket with the AF_NETLINK domain

        fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
        if (fd == -1)
        {
            std::cout << "Can't open socket" << std::endl;
        }

        sa.nl_family = AF_NETLINK;
        len = get_ip(AF_INET); // To get ipv6, use AF_INET6 instead
        char buf[BUFLEN];
        uint32_t nl_msg_type;
        do
        {
            len = get_msg(fd, &sa, buf, BUFLEN);
            // check(len);

            nl_msg_type = parse_nl_msg(buf, len);
        } while (nl_msg_type != NLMSG_DONE && nl_msg_type != NLMSG_ERROR);
    }
    ~InfoCollertor()
    {
        close(fd);
    }
    std::list<IfDescr> ifDescrList()
    {
        return this->_if_descr_list;
    }

private:
    const uint16_t BUFLEN = 4096;
    int fd = 0;
    int len = 0;
    struct sockaddr_nl sa = {0};
    char buf[4096];
    std::list<IfDescr> _if_descr_list = {};

    int get_ip(int domain)
    {

        // assemble the message according to the netlink protocol
        std::fill(buf, buf + 4096, 0);
        struct nlmsghdr *nl;
        nl = (struct nlmsghdr *)buf;
        nl->nlmsg_len = NLMSG_LENGTH(sizeof(struct ifaddrmsg));
        nl->nlmsg_type = RTM_GETADDR;
        nl->nlmsg_flags = NLM_F_REQUEST | NLM_F_ROOT;

        struct ifaddrmsg *ifa;
        ifa = (struct ifaddrmsg *)NLMSG_DATA(nl);
        ifa->ifa_family = domain; // we only get ipv4 address here

        // prepare struct msghdr for sending.
        struct iovec iov = {nl, nl->nlmsg_len};
        struct msghdr msg = {&sa, sizeof(sa), &iov, 1, NULL, 0, 0};

        // send netlink message to kernel.
        int r = sendmsg(fd, &msg, 0);
        return (r < 0) ? -1 : 0;
    }
    int get_msg(int fd, struct sockaddr_nl *sa, void *buf, size_t len)
    {
        struct iovec iov;
        struct msghdr msg;
        iov.iov_base = buf;
        iov.iov_len = len;

        memset(&msg, 0, sizeof(msg));
        msg.msg_name = sa;
        msg.msg_namelen = sizeof(*sa);
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;

        return recvmsg(fd, &msg, 0);
    }
    uint32_t parse_nl_msg(void *buf, size_t len)
    {
        struct nlmsghdr *nl = NULL;

        for (nl = (struct nlmsghdr *)buf;
             NLMSG_OK(nl, (uint32_t)len) && nl->nlmsg_type != NLMSG_DONE;
             nl = NLMSG_NEXT(nl, len))
        {
            if (nl->nlmsg_type == NLMSG_ERROR)
            {
                printf("error");
                return -1;
            }

            if (nl->nlmsg_type == RTM_NEWADDR)
            {
                struct ifaddrmsg *ifa;
                ifa = (struct ifaddrmsg *)NLMSG_DATA(nl);
                _if_descr_list.push_back(parse_ifa_msg(ifa, IFA_RTA(ifa), IFA_PAYLOAD(nl)));
                continue;
            }
        }
        return nl->nlmsg_type;
    }
    IfDescr parse_ifa_msg(struct ifaddrmsg *ifa, void *buf, size_t len)
    {
        char ifname[IF_NAMESIZE];
        std::string address;
        std::string broadcast;
        std::string mac_addr;

        std::string ifname_s(if_indextoname(ifa->ifa_index, ifname));
        uint8_t prefix(ifa->ifa_prefixlen);

        struct rtattr *rta = NULL;
        int fa = ifa->ifa_family;
        for (rta = (struct rtattr *)buf; RTA_OK(rta, len); rta = RTA_NEXT(rta, len))
        {
            if (rta->rta_type == IFA_ADDRESS)
            {
                address = std::string(ntop(fa, RTA_DATA(rta)));
            }

            if (rta->rta_type == IFLA_ADDRESS)
            {
                char addr_buf[64];
                unsigned char *c = (unsigned char *)RTA_DATA(rta);

                snprintf(addr_buf, 64, "%02x:%02x:%02x:%02x:%02x:%02x",
                         c[0], c[1], c[2], c[3], c[4], c[5]);
                mac_addr = addr_buf;
            }

            if (rta->rta_type == IFA_BROADCAST)
            {
                broadcast = std::string(ntop(fa, RTA_DATA(rta)));
            }
        }
        return IfDescr(ifname_s, address, broadcast, prefix, mac_addr);
    }
    char *ntop(int domain, void *buf)
    {

        static char ip[INET_ADDRSTRLEN];
        inet_ntop(domain, buf, ip, INET_ADDRSTRLEN);
        return ip;
    }
};

int main(void)
{

    InfoCollertor ic;
    for (auto item : ic.ifDescrList())
        std::cout << "name: " << item.ifname() << "\n address:  " << item.ipv4() << "\n prefix:  "
                  << unsigned(item.prefix()) << "\n broadcast: "
                  << item.broasdcast() << "\n MAC: " << item.mac_aadr() << std::endl;

    return 0;
}
*/
