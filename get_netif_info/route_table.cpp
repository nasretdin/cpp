#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <memory.h>
#include <unistd.h>
#include <errno.h>

#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <net/if.h>
#include <arpa/inet.h>

#ifndef DEBUG
#define DEBUG 0
#endif

#define printf_debug(fmt, ...)          \
    ({                                  \
        if (DEBUG)                      \
            printf(fmt, ##__VA_ARGS__); \
    })

struct nlrtmsg
{
    struct nlmsghdr hdr;
    struct rtmsg msg;
} __attribute__((packed));

void print_intf(unsigned int ifindex, const char *text, char *buffer)
{
    printf("%s: %d %s\n", text, ifindex, if_indextoname(ifindex, buffer));
}

void print_rta_intf(struct rtattr *rta, const char *text, char *buffer)
{
    print_intf(*(unsigned *)RTA_DATA(rta), text, buffer);
}

void print_rta_addr(struct rtattr *rta, const char *text, uint8_t family, char *buffer, size_t size)
{
    printf("%s: %s\n", text, inet_ntop(family, RTA_DATA(rta), buffer, size));
}

void print_rta_multipath(struct rtattr *rta, uint8_t family);

void print_rta_entries(struct rtattr *rta, size_t len, uint8_t family, int tab)
{
    for (; RTA_OK(rta, len); rta = RTA_NEXT(rta, len))
    {
        switch (rta->rta_type)
        {
        case RTA_OIF:
        {
            char ifname[IF_NAMESIZE];
            print_rta_intf(rta, "\tInterface" + (!tab), ifname);
            break;
        }
        case RTA_DST:
        {
            char buffer[INET6_ADDRSTRLEN];
            print_rta_addr(rta, "\tDestination" + (!tab), family, buffer, sizeof(buffer));
            break;
        }
        case RTA_GATEWAY:
        {
            char buffer[INET6_ADDRSTRLEN];
            print_rta_addr(rta, "\tGateway" + (!tab), family, buffer, sizeof(buffer));
            break;
        }
        case RTA_MULTIPATH:
        {
            print_rta_multipath(rta, family);
            break;
        }
        case RTA_METRICS:
        {
            char buffer[INET6_ADDRSTRLEN + INET6_ADDRSTRLEN];
            print_rta_addr(rta, "\t RTA_METRICS" + (!tab), family, buffer, sizeof(buffer));
            break;
        }
        default:
        {
            printf_debug("rta type %d\n", rta->rta_type);
            break;
        }
        }
    }
}

void print_rta_multipath(struct rtattr *rta, uint8_t family)
{
    printf("Multipath record\n");
    struct rtnexthop *rtnh = (struct rtnexthop *)RTA_DATA(rta);
    int len = RTA_PAYLOAD(rta);
    for (; RTNH_OK(rtnh, len); len -= rtnh->rtnh_len, rtnh = RTNH_NEXT(rtnh))
    {
        struct rtattr *rta = RTNH_DATA(rtnh);
        size_t rtalen = rtnh->rtnh_len - sizeof(*rtnh);
        print_rta_entries(rta, rtnh->rtnh_len - sizeof(*rtnh), family, 1);

        char ifname[IF_NAMESIZE];
        print_intf(rtnh->rtnh_ifindex, "\tInterface", ifname);
    }
}

void print_route(struct rtmsg *rtm, size_t len)
{
    if (rtm->rtm_family == AF_INET)
    {
        printf("IPv4\n");
    }
    else if (rtm->rtm_family == AF_INET6)
    {
        printf("IPv6\n");
    }
    else
    {
        printf_debug("Unknown address family\n");
        return;
    }

    struct rtattr *rta = RTM_RTA(rtm);
    print_rta_entries(rta, len - sizeof(*rtm), rtm->rtm_family, 0);
    putchar('\n');
}

void send_getroute_req(int fd)
{
    struct nlmsghdr hdr = {
        .nlmsg_len = sizeof(struct nlrtmsg),
        .nlmsg_type = RTM_GETROUTE,
        .nlmsg_flags = NLM_F_REQUEST | NLM_F_ACK | NLM_F_DUMP | NLM_F_ROOT,
    };
    struct nlrtmsg req = {
        .hdr = hdr,
        .msg = {},
    };
    struct iovec iov = {
        .iov_base = &req,
        .iov_len = sizeof(req),
    };
    struct sockaddr_nl dst = {
        .nl_family = AF_NETLINK,
    };
    struct msghdr reqhdr = {
        .msg_name = &dst,
        .msg_namelen = sizeof(dst),
        .msg_iov = &iov,
        .msg_iovlen = 1,
    };

    if (sendmsg(fd, &reqhdr, 0) < 0)
    {
        fprintf(stderr, "sendmsg fail %s\n", strerror(errno));
        exit(1);
    }
}

void recv_getroute_res(int fd)
{
    uint8_t buffer[8192];

    struct iovec iov = {
        .iov_base = buffer,
        .iov_len = sizeof(buffer),
    };
    struct sockaddr_nl from = {
        .nl_family = AF_NETLINK,
    };
    struct msghdr reshdr = {
        .msg_name = &from,
        .msg_namelen = sizeof(from),
        .msg_iov = &iov,
        .msg_iovlen = 1,
    };

    int len;
    struct nlmsghdr *hdr;
    while (1)
    {
        if ((len = recvmsg(fd, &reshdr, 0)) <= 0)
        {
            fprintf(stderr, "recvmsg fail %s\n", strerror(errno));
            exit(1);
        }
        for (hdr = (struct nlmsghdr *)buffer; NLMSG_OK(hdr, len); hdr = NLMSG_NEXT(hdr, len))
        {
            printf_debug("nlmsg type %d\n", hdr->nlmsg_type);
            switch (hdr->nlmsg_type)
            {
            case NLMSG_DONE:
                return;
            case RTM_NEWROUTE:
                print_route((struct rtmsg *)NLMSG_DATA(hdr), hdr->nlmsg_len - NLMSG_HDRLEN);
                break;
            default:
                break;
            }
        }
    }
}

int main()
{
    int fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    if (fd < 0)
    {
        exit(1);
    }

    struct sockaddr_nl sa = {
        .nl_family = AF_NETLINK,
    };
    if (bind(fd, (struct sockaddr *)&sa, sizeof(sa)) < 0)
    {
        close(fd);
        exit(1);
    }

    send_getroute_req(fd);
    recv_getroute_res(fd);

    close(fd);

    return 0;
}