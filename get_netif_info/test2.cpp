#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <sys/types.h>
#include <sys/socket.h>

#define MAX_PAYLOAD 2048

int main() {
    int nl_socket;
    struct sockaddr_nl src_addr, dest_addr;
    struct nlmsghdr *nlh;
    struct iovec iov;
    struct msghdr msg;

    // Create a Netlink socket
    nl_socket = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    if (nl_socket == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // Initialize the source and destination addresses
    memset(&src_addr, 0, sizeof(src_addr));
    src_addr.nl_family = AF_NETLINK;
    src_addr.nl_pid = getpid();
    src_addr.nl_groups = 0;  // unicast

    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.nl_family = AF_NETLINK;
    dest_addr.nl_pid = 0;  // Kernel
    dest_addr.nl_groups = 0;  // unicast

    // Bind the socket
    bind(nl_socket, (struct sockaddr*)&src_addr, sizeof(src_addr));

    // Prepare and send the Netlink request
    nlh = (struct nlmsghdr*)malloc(NLMSG_SPACE(MAX_PAYLOAD));
    memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
    nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
    nlh->nlmsg_type = RTM_GETLINK;
    nlh->nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;

    iov.iov_base = (void*)nlh;
    iov.iov_len = nlh->nlmsg_len;

    memset(&msg, 0, sizeof(msg));
    msg.msg_name = (void*)&dest_addr;
    msg.msg_namelen = sizeof(dest_addr);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    // Send the request
    sendmsg(nl_socket, &msg, 0);

    // Receive and process responses
    while (1) {
        memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
        recvmsg(nl_socket, &msg, 0);

        if (nlh->nlmsg_type == NLMSG_DONE)
            break;

        if (nlh->nlmsg_type == NLMSG_ERROR) {
            perror("Netlink error");
            exit(EXIT_FAILURE);
        }

        struct ifinfomsg *ifi_info = (struct ifinfomsg*)NLMSG_DATA(nlh);


        printf("Interface Index: %d\n", ifi_info->ifi_index);
    }

    free(nlh);

    // Close the Netlink socket
    close(nl_socket);

    return 0;
}