#ifndef DTOs_hpp
#define DTOs_hpp

#include "oatpp/macro/codegen.hpp"
#include "oatpp/Types.hpp"

#include OATPP_CODEGEN_BEGIN(DTO)

/**
 *  Data Transfer Object. Object containing fields only.
 *  Used in API for serialization/deserialization and validation
 */
class MyDto : public oatpp::DTO {
  
  DTO_INIT(MyDto, DTO)
  DTO_FIELD(Int32, statusCode);
  DTO_FIELD(String, message);
  
};

class IPv4Info : public oatpp::DTO {
    DTO_INIT(IPv4Info, DTO);
    DTO_FIELD(String, if_name);
    DTO_FIELD(String, ipv4add);
    DTO_FIELD(String, broadcat);
    DTO_FIELD(Int8, prefix);
    DTO_FIELD(String, mac_addr);
};

class IfIPv4InfoList: public oatpp::DTO{
    DTO_INIT(IfIPv4InfoList, DTO);
    DTO_FIELD(Vector<Object<IPv4Info>>, ifinfolist);
};


class IPv6Info : public oatpp::DTO {
    DTO_INIT(IPv6Info, DTO);
    DTO_FIELD(String, if_name);
    DTO_FIELD(String, ipv6add);
};


class IfIPv6InfoList: public oatpp::DTO{
    DTO_INIT(IfIPv4InfoList, DTO);
    DTO_FIELD(Vector<Object<IPv6Info>>, ifinfolist);
};

#include OATPP_CODEGEN_END(DTO)

#endif /* DTOs_hpp */
