﻿class Logger : public std::ofstream {

public:
	Logger(const Logger& log) = delete;
	static Logger* logger(const char* logFileName) {

		if (instancePtr == nullptr) {
			instancePtr = new Logger(logFileName);
			return instancePtr;
		}
		else {
			return instancePtr;
		}
	}
	Logger& Error();

private:
	static Logger* instancePtr;
	const char* logFileName;
	std::string _getTimeStr();
	Logger(const char* fileName);
	~Logger();
};
