﻿#include<iostream>
#include  <vector>
#include  <string>
#include  <algorithm>
#include  <fstream>
#include  <chrono>
#include "logger.h"



Logger* Logger::instancePtr = nullptr;
Logger::Logger(const char* fileName) : logFileName(fileName)
{
	open(logFileName, std::ofstream::app);
}

Logger::~Logger()
{
	close();
}

Logger& Logger::Error() {
	auto str_time = _getTimeStr();
	*instancePtr << '\n' << str_time << " [ERROR] ";
	return *instancePtr;
}

std::string Logger::_getTimeStr() {
	time_t rawtime;
	struct tm* timeinfo;
	char buffer[80];
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(buffer, sizeof(buffer), "%d-%m-%Y  %H:%M:%S", timeinfo);
	std::string str_time(buffer);
	return str_time;
}



int main(int argc, char* argv[]) {

	Logger* logger = Logger::logger("log.txt");
	logger->Error() << "error msg";
	std::cout << std::endl;

}





