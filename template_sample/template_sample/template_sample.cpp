﻿#include <iostream>
#include <string>
#include <tuple>

using namespace std;

template <typename T, typename V> 
class Person {
public:
  Person(T name, V age) {
    f_name = name;
    f_age = age;
  }

  auto get_name() const { return &f_name; }
  auto get_age() const { return &f_age; }

private:
  T f_name;
  V f_age;
};

template <typename T> T myMax(T x, T y) {
  // bool result = false;
  if (x > y) {
    return x;
  } else {
    return y;
  }
}


struct Record {
  std::string name;
  unsigned int floor;
  double weight;
};

ostream &operator<<(ostream &out, const Record &obj) {
  out << obj.name << ":" << obj.floor << " | " << obj.weight << std::endl;
  return out;
}

bool operator>(const Record &l, const Record &r) {
  return std::tie(l.name, l.floor, l.weight) >
         std::tie(r.name, r.floor, r.weight); // keep the same order
}

template <typename T, typename V>
ostream &operator<<(ostream &out, const Person<T, V> &obj) {
  out << obj.get_name() << ":" << obj.get_age() << std::endl;
  return out;
}

int main() {

  //std::cout << myMax<int>(3, 8) << std::endl;
  //std::cout << myMax<char>('a', 'f') << std::endl;

  //Record r1{"R1", 1, 100};
  //Record r2{"R2", 2, 200};
  //auto m = myMax(r1, r2);

  Person<std::string, int> vasya{"Vasek", 30};
  std::cout << *vasya.get_name() << std::endl;
  std::cout << *vasya.get_age() << std::endl;

  Person<std::string, std::string> petya{"Petruha", "20"};
  std::cout << *petya.get_name() << std::endl;
  std::cout << *petya.get_age() << std::endl;

  std::cout << &petya << std::endl;
  std::cout << &vasya << std::endl;

  Person<std::string, Record> ilya{"Ilya", {"R1", 1, 100}};
  std::cout << ilya << std::endl;
  //  std::cout << ilya.get_age() << std::endl;

  return 0;
}
