cmake_minimum_required(VERSION 3.5)
project(coasrv)


set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
add_compile_options(-Wall -Wextra)

add_subdirectory(lib)
add_subdirectory(app)
