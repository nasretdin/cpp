#include <iostream>
#include "../config.h"
#include <ifaddrs.h>

int main()
{

    struct ifaddrs *ptr_ifaddrs = nullptr;

    int result = getifaddrs(&ptr_ifaddrs);
    if (result != 0)
    {
        std::cout << "CVan not get IP addresses. " << strerror(errno) << std::endl;

        return 1;
    }
    struct ifaddrs *addr_prt;
    addr_prt = ptr_ifaddrs->ifa_next;
    std::string sys_address;
    std::list<std::string> sys_addresses;
    while (addr_prt)
    {
        sa_family_t address_family = addr_prt->ifa_addr->sa_family;
        if (address_family == AF_INET)
        {
            if (addr_prt->ifa_addr != nullptr)
            {
                char buffer[INET_ADDRSTRLEN] = { 0,};
                inet_ntop(
                    AF_INET,
                    &((struct sockaddr_in *)(addr_prt->ifa_addr))->sin_addr,
                    buffer,
                    INET_ADDRSTRLEN);

                sys_address = std::string(buffer);
                sys_addresses.push_back(sys_address);
            }
            
        }
        addr_prt = addr_prt->ifa_next;
    }

    freeifaddrs(ptr_ifaddrs);
    return 0;

    // Config *config = Config::getConfig("coasrv.ini");

    std::cout << "hi\n";
}