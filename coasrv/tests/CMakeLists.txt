cmake_minimum_required(VERSION 3.5)

project(config_test LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
target_link_directories(config_test PRIVATE /home/ray/projects/cpp/coasrv)


add_executable(config_test config_test.cpp)