#pragma once
#include "common_types.h"
#include <cstdint>
#include <stdexcept>
#include <vector>
#include <cstdint>

const int HEADER_SIZE = 20;

#pragma once

enum class CoAPacketTypes : uint8_t {
  UnknownType = 0x00,
  DisconnectRequest = 0x40,
  DisconnectACK = 0x41,
  DisconnectNAK = 0x42,
  CoARequest = 0x43,
  CoAACK = 0x44,
  CoANAK = 0x45
};

enum class Attributes : uint8_t {
  // https://www.rfc-editor.org/rfc/rfc2865.html#page-22
  // https://www.iana.org/assignments/radius-types/radius-types.xhtml
  UnknownAttr = 0x00,
  UserName = 0x01,
  NASIPAddress = 0x04,
  FramedIPAddress = 0x08,
  FilterId = 0x11

};

class AVPair {
	CoAPacketTypes Type;
	uint8_t Len;
	uint8_t Value[];

public:
	CoAPacketTypes Tag() const { return Type; };
	const AVPair* Next() const { return (AVPair*)((uint8_t*)this+Len); }
	const std::string_view Data() const { return std::string_view((const char*)Value, Len-2); }
};

struct PayloadHdr {
	CoAPacketTypes Code;
	uint8_t Id;
	uint16_t Len;
	uint8_t Auth[16]{};
	uint16_t Length() const {
		return ((Len & 0xFF) << 8) | uint8_t(Len >> 8);
	}
	const AVPair* Payload() const { return (AVPair*)this+1; }
	explicit PayloadHdr(const std::vector<uint8_t>& header);

private:
	static CoAPacketTypes getPayloadType(uint8_t value);
};

static_assert(sizeof(PayloadHdr)==HEADER_SIZE, "Payload header must be 20 byte size.");

class CoARequest {
	PayloadHdr payloadhdr;
	explicit CoARequest(const std::vector<uint8_t>& request);

};