﻿#include <utility>
#include <vector>
#include <array>
#include "coaserver.h"
#include "request.h"


//std::string_view;
/*
 * */

class AttrValuePair {
    // https://www.rfc-editor.org/rfc/rfc2865.html#page-22
public:
    Attributes Type;
    std::vector<uint8_t> Len;
    std::vector<uint8_t> Value;

    explicit AttrValuePair(std::vector<uint8_t> raw_avp_data) {
        Type = getAttrByValue(raw_avp_data.at(0));
        Len.push_back(raw_avp_data.at(1));
        std::copy(raw_avp_data.begin() + 2, raw_avp_data.end(), std::back_inserter(Value));
    }

    static Attributes getAttrByValue(uint8_t value) {
        switch (value) {
            case 1: return Attributes::UserName;
            case 4: return Attributes::NASIPAddress;
            case 8: return Attributes::FramedIPAddress;
            case 11: return Attributes::FilterId;
            default: return Attributes::UnknownAttr;

        }
    }
};

class AttrListMaker {
    std::vector<AttrValuePair> attr_list;

public:
    AttrListMaker() = delete;

    explicit AttrListMaker(std::vector<uint8_t> attr_data) {
        uint8_t attr_len = 0;
        uint8_t start_attr_pos = 0;
        uint8_t end_attr_pos = 0;
        while (end_attr_pos < attr_data.size()) {
            attr_len = attr_data.at(attr_len + 1);
            end_attr_pos += attr_len + start_attr_pos;
            std::vector<uint8_t> raw_attr;
            std::move(attr_data.begin() + start_attr_pos, attr_data.begin() + start_attr_pos + attr_len,
                    std::back_inserter(raw_attr));
            attr_list.push_back(makeAttrValuePair(raw_attr));
            start_attr_pos += attr_len;
        }
    }

    static AttrValuePair makeAttrValuePair(std::vector<uint8_t> attr_data) {
        AttrValuePair attr_value_pair(std::move(attr_data));
        return attr_value_pair;
    }
};

PayloadHdr::PayloadHdr(const std::vector<uint8_t>& header) {
    auto code = getPayloadType(header.at(0));
    if (code != CoAPacketTypes::UnknownType) {
        Code = code;
    } else {
        throw std::invalid_argument("Invalid request code ");
    }
    if (header.at(1) != 0) {
        Id = header.at(1);
    } else {
        throw std::invalid_argument("Request ID is zero.");
    }
    Len = header.at(3), header.at(4);
    std::copy(header.begin() + 4, header.end(), Auth);
}

CoAPacketTypes PayloadHdr::getPayloadType(uint8_t value) {
    switch (value) {
        case 40:return CoAPacketTypes::DisconnectRequest;
        case 41:return CoAPacketTypes::DisconnectACK;
        case 42:return CoAPacketTypes::DisconnectNAK;
        case 43:return CoAPacketTypes::CoARequest;
        case 44:return CoAPacketTypes::CoAACK;
        case 45:return CoAPacketTypes::CoANAK;
        default:return CoAPacketTypes::UnknownType;
    }
}

CoARequest::CoARequest(const std::vector<uint8_t>& request) : payloadhdr(request) {

}
