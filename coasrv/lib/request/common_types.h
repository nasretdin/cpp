#include <cstdint>

class u16_BE {

public:
    constexpr u16_BE() { }

    constexpr explicit u16_BE(uint8_t val)
    {

        Value[0] = (val >> 8) & 0xFF;

        Value[1] = (val >> 0) & 0xFF;
    }

    void Verify() const { }

    constexpr operator uint16_t() const { return (Value[0] << 8)+Value[1]; }

private:
    uint8_t Value[2]{0};
};