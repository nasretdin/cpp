#include <iostream>
#include "config.h"


const std::string MSG_SOCKET_CREATE_FAIL = "Can not create socket";
const std::string MSG_BIND_SOCKET_FAIL = "Can not bind to socket";

class CoAServer {
public:
    explicit CoAServer(const char* configFileName);
    void  doRun();

private:
    Config coaConfig;
	int socketDesc;
	struct sockaddr_in serv_sock_in;

    void makeResponse(char* request, size_t request_len, std::string_view& response);
};
