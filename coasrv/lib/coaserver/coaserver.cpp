#include "coaserver.h"
#include "request.h"
#include <ifaddrs.h>
#include <arpa/inet.h>

void CoAServer::doRun() {

	while (true) {
        socklen_t len;
        struct sockaddr_in client_sock_in;
        char buffer[coaConfig.PayLoadSz.Val];
        ssize_t n;
        std::string_view response;

        len = sizeof(client_sock_in);  //len is value/result

        n = recvfrom(socketDesc, (char *)buffer, coaConfig.PayLoadSz.Val,
                MSG_WAITALL, ( struct sockaddr *) &client_sock_in,
                &len);
        buffer[n] = '\0';
        printf("Client : %s\n", buffer);
        makeResponse(buffer, n, response);
        sendto(socketDesc, (const char *)response.data(), response.length(),
                MSG_CONFIRM, (const struct sockaddr *) &client_sock_in,
                len);
        std::cout<<"Hello message sent."<<std::endl;
	}
}

CoAServer::CoAServer(const char* configFileName)
		:coaConfig(configFileName) {

	if ((socketDesc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1) {
		throw std::runtime_error(MSG_SOCKET_CREATE_FAIL);
	}

	memset((char*)&serv_sock_in, 0, sizeof(serv_sock_in));

    serv_sock_in.sin_family = AF_INET;
    serv_sock_in.sin_port = htons(coaConfig.PortNum.Val);
    //serv_sock_in.sin_addr.s_addr = coaConfig.Address.Val;
    serv_sock_in.sin_addr.s_addr = INADDR_ANY;
	//bind socket to portNum
	if (bind(socketDesc, (struct sockaddr*)&serv_sock_in, sizeof(serv_sock_in))==-1) {
		throw std::runtime_error(MSG_BIND_SOCKET_FAIL);
	}
}


void CoAServer::makeResponse(char* request, size_t request_len, std::string_view& response) {
	int i = 0;
	std::vector<uint8_t> request_v(request, request + request_len);

	auto* hdr = reinterpret_cast<PayloadHdr*>(request);
    response = "\x29\x21\x00\x14\x34\x1b\x57\x6a\x19\xde\x47\x22\x3e\x07\xf4\x06\x27\x63\x1f\x2d";
	std::cout << request;

}
