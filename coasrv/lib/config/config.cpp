#include <filesystem>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include "config.h"

//================Config::===================================
void Config::trimStr(std::string &str_for_trim) {
  str_for_trim.erase(str_for_trim.find_last_not_of(' ') + 1);
  str_for_trim.erase(0, str_for_trim.find_first_not_of(' '));
}

Config::Config(const char *filename) {
  std::ifstream input_file(filename, std::ios::in);
  if (!input_file) {
	throw std::runtime_error(std::string("Can not open config file ") + filename);
  }
  std::string line;
  while (std::getline(input_file, line)) {
	std::string key;
	std::string val;
	getKeyValuePair(line, key, val);
	auto found = Reg.Find(key);
	if (!found)
	  throw std::runtime_error("Unknown key...");
	found->Transform(val);
  }
  if (!Reg.AllFieldsProcessed())
	throw "...";
}

void Config::getKeyValuePair(std::string_view raw, std::string &key, std::string &value) {
  size_t found = raw.find('=');
  if (found!=std::string::npos) {
	key = raw.substr(0, found);
	trimStr(key);
	value = raw.substr(raw.find('=') + 1, raw.size() - 1);
	trimStr(value);
  }
}
bool Config::validateSecret() const {
  if (Secret.Val.length() < 8) {
	return false;
  }
  return true;
}
bool Config::validatePort() const {
  if (0 > PortNum.Val | PortNum.Val > 65535) {
	return false;
  }
  return true;
}

//================
//================IPAddress::=================================
//IPAddress::IPAddress(std::string& str_ip_address) {
//	if (0>inet_pton(AF_INET, const_cast<char*>(str_ip_address.c_str()), &ip_address)) {
//		throw std::invalid_argument(MSG_INVALID_ADDRESS);
//	}
//}
//
//
//in_addr IPAddress::ipaddress() {
//	return ip_address;
//}
void IPAddrValue::DoTransform(const std::string &val) {
  if (0 > inet_pton(AF_INET, const_cast    <char *>(val.c_str()), &Val)) {
	throw
		std::invalid_argument(MSG_INVALID_ADDRESS);
  }
}

//================
void ValueRef::Registry::Add(ValueRef *value_ref) {
  for (const auto &entrie : Entries) {
	if (entrie->Key==value_ref->Key)
	  if (entrie->Key==value_ref->Key)
		throw std::logic_error("Duplicate key.");
  }
  Entries.push_back(value_ref);
}

ValueRef *ValueRef::Registry::Find(const std::string &key) {
  for (const auto &e : Entries) {
	if (e->Key==key)
	  return e;
  }
  return nullptr;
}

void ValueRef::Registry::Remove(ValueRef *value_ref) {
  std::remove_if(Entries.begin(), Entries.end(), [&](const auto &item) {
	return item==value_ref;
  });

}

bool ValueRef::Registry::AllFieldsProcessed() {
  for (auto e : Entries) {
	if (!e->Processed)
	  return false;
  }
  return true;
}


void Config::gatherLocalIPv4Addresses(std::string &ipv4addresses) {
  struct ifaddrs *ptr_ifaddrs = nullptr;
  int result = getifaddrs(&ptr_ifaddrs);
  if (result!=0) {
	std::cout << "`getifaddrs()` failed: " << strerror(errno) << std::endl;
	return;
  }
  for (
	  struct ifaddrs *ptr_entry = ptr_ifaddrs;
	  ptr_entry!=nullptr;
	  ptr_entry = ptr_entry->ifa_next) {
	std::string ipaddress_human_readable_form;
	std::string netmask_human_readable_form;

	std::string interface_name = std::string(ptr_entry->ifa_name);
	sa_family_t address_family = ptr_entry->ifa_addr->sa_family;
	if (address_family==AF_INET) {
	  if (ptr_entry->ifa_addr!=nullptr) {
		char buffer[INET_ADDRSTRLEN] = {
			0,
		};
		inet_ntop(
			address_family,
			&((struct sockaddr_in *)(ptr_entry->ifa_addr))->sin_addr,
			buffer,
			INET_ADDRSTRLEN);

		ipaddress_human_readable_form = std::string(buffer);
	  }
	  ipv4addresses += ipaddress_human_readable_form + ";";
	} else {
	  //throw std::exception();  // FIXME if ipv6 address?
	  // AF_UNIX, AF_UNSPEC, AF_PACKET etc.
	  // If ignored, delete this section.
	}
  }
}

bool Config::isAddressLocalExist() {
  std::string ip_addrs;

  try {
	gatherLocalIPv4Addresses(ip_addrs);
  }
  catch (const std::exception &e) {
	std::cerr << e.what() << '\n';
  }
  char buff[INET_ADDRSTRLEN] = {};
  return (ip_addrs.find(std::string(buff))!=std::string::npos);
}


bool Config::validateIPv4Address() {
  if (!isAddressLocalExist()) {
	throw std::invalid_argument(MSG_NON_LOCAL_IP_ADDRESS);
  }
//  if (0>inet_pton(AF_INET, const_cast<char*>(Address.Val), &ip_address)) {
//		throw std::invalid_argument(MSG_INVALID_ADDRESS);
//	}
  return true;
}