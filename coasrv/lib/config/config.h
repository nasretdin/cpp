#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <list>
#include <algorithm>
#include <functional>
#include <regex>
#include <utility>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string_view>

const std::string MAX_PAYLOAD_SIZE_CONST = "MAX_PAYLOAD_SIZE";
const std::string PORT_CONST = "PORT";
const std::string SECRET_CONST = "SECRET";
const std::string ADDRESS_CONST = "ADDRESS";

const std::string MSG_MAX_PAYLOAD_SIZE_INVALID = "Invalid value for MAX_PAYLOAD_SIZE parameter was in config file";
const std::string MSG_PORT_NUM_NOT_RANGE = "Invalid value for parameter PORT was set in config file.";
const std::string MSG_PORT_NUM_NOT_INT = "Non number value for PORT parameter was set in config file.";
const std::string MSG_INVALID_ADDRESS = "Invalid value for ADDRESS parameter was set in config file.";
const std::string MSG_SECRET_TOO_SHORT = "The SECRET is too short.";
const std::string MSG_NON_LOCAL_IP_ADDRESS = "The ADDRESS parameter is not local IP-address";

using ValidateFn = std::function<bool()>;

inline bool NoValidation() { return true; }

class ValueRef {
public:
  class Registry {
  private:
	std::list<ValueRef *> Entries;
  public:
	Registry() = default;

	void Add(ValueRef *value_ref);

	ValueRef *Find(const std::string &key);

	void Remove(ValueRef *value_ref);

	bool AllFieldsProcessed();
  };

  explicit ValueRef(Registry &reg, const char *key, ValidateFn validate)
	  : Key(key), Reg(reg), Validate(std::move(validate)) {
	reg.Add(this);
  }

  void Transform(const std::string &val) {
	if (Processed)
	  throw std::runtime_error("Duplicate parameter: " + std::string(ValueRef::Key));

	try {
	  DoTransform(val);
	}

	catch (const std::exception &err_msg) {
	  throw std::invalid_argument("Invalid value for parameter: " + std::string(ValueRef::Key));
	}

	if (!Validate())
	  throw std::runtime_error("Validation of the """ + std::string(Key) + """ parameter failed.");
	Processed = true;
  };

  virtual ~ValueRef() {
	Reg.Remove(this);
  };
protected:
  std::string Key;
  Registry &Reg;
  ValidateFn Validate;
  bool Processed = false;
private:
  virtual void DoTransform(const std::string &val) = 0;
};

class U_16Value : public ValueRef {
public:
  U_16Value(Registry &reg, const char *key, ValidateFn validate = NoValidation)
	  : ValueRef(reg, key, std::move(validate)) {}

  void DoTransform(const std::string &val) override {
	Val = std::stoi(val);
  }

  uint16_t Val{};
};

class IPAddrValue : public ValueRef {
public:
  IPAddrValue(Registry &reg, const char *key, ValidateFn validate = NoValidation)
	  : ValueRef(reg, key, std::move(validate)) {}

  void DoTransform(const std::string &val) override;

  in_addr_t Val{};
};

class StrValue : public ValueRef {
public:
  StrValue(Registry &reg, const char *key, ValidateFn validate = NoValidation)
	  : ValueRef(reg, key, std::move(validate)) {}

  void DoTransform(const std::string &val) override {
	Val = val;
  }

  std::string Val;
};

class Config {
  ValueRef::Registry Reg;
public:
  explicit Config(const char *filename);

  U_16Value PayLoadSz = {Reg, "MAX_PAYLOAD_SIZE"};
  U_16Value PortNum = {Reg, "PORT", [this]() { return validatePort(); }};
  StrValue Secret = {Reg, "SECRET", [this]() { return validateSecret(); }};
  IPAddrValue Address = {Reg, "ADDRESS", [this](){return validateIPv4Address();}};
private:
  [[nodiscard]] bool validateSecret() const;
  [[nodiscard]] bool validatePort() const;
  [[nodiscard]] static bool validateIPv4Address() ;

  static void trimStr(std::string &str_for_trim);

  static void getKeyValuePair(std::string_view  raw, std::string &key, std::string &value);
  static void gatherLocalIPv4Addresses(std::string& ipv4addresses);
  static bool isAddressLocalExist() ;

};
