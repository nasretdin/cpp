#include <iostream>
#include <vector>
#include "coaserver.h"

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cout << "Configuration file is not specified.\n";
        return -1;
    }
    try {
        CoAServer server(argv[1]);
		server.doRun();
    } catch (const std::exception &e) {
        std::cerr << e.what() << '\n';
    }
    return -2;
}
