﻿#include <iostream>
#include <array>
#include<algorithm>

void FindIndex(std::array<uint16_t, 10> &numbers, uint16_t key) {
    std::sort(numbers.begin(), numbers.end());

    uint16_t key_index = 0;
    uint16_t left = 0;
    uint16_t rigth = numbers.size();


    while (true) {
        if (key == numbers.at(key_index)) {
            std::cout << "\nThe key: " << key << " found at: " << key_index + 1 << " position. \n";
            return;
        }
        if (left == rigth - 1 || left == rigth) {
            std::cout << "Key not found.\n";
            return;
        }
        if (key > numbers.at(key_index)) { // to right
            left = key_index;
            key_index = key_index + ((rigth - key_index) / 2);
            continue;
        }
        if (key < numbers.at(key_index)) { //to left
            rigth = key_index;
            key_index = key_index - ((left + key_index)) / 2;
            continue;
        }
    }
}


int main() {
    std::array<uint16_t, 10> arr_numbers = {2, 5, 22, 6, 1, 8, 4, 9, 3, 10};

    std::sort(arr_numbers.begin(), arr_numbers.end());
    for (auto num: arr_numbers) {
        std::cout << num << " | ";
    }
    std::cout << std::endl;

    FindIndex(arr_numbers, 25);
    FindIndex(arr_numbers, 23);
    FindIndex(arr_numbers, 0);
    FindIndex(arr_numbers, -6);

    for (auto num: arr_numbers) {
        FindIndex(arr_numbers, num);
    }
}

