#include <iostream>

template <typename... Elems>
auto sum(const Elems&... elems){
    return (elems + ...);
}

template <typename... Elems>
void print(const Elems&... elems){
    (std::cout << ... << elems);
    std::cout << std::endl;
    //with spaces
    ((std::cout <<  elems <<  ' '),  ...);

}

int main(){
    sum(1,2,4,6);
    print(1,2,3,4);
}