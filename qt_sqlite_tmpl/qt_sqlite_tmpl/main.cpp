#include "mainwindow.h"

#include "lib/dbmanager.h"
#include <QApplication>

static const QString path = "example.db";

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  DbManager db_mgr(path);

  if (db_mgr.isOpen()) {
    //      db.createTable();   // Creates a table if it doens't exist.
    //      Otherwise, it will use existing table.
    Person person("Jim", "Beam");
    DbObjBase &personTbl = person;
    db_mgr.makeTable(personTbl);
    db_mgr.insert(person);
    Person out_person;
    db_mgr.selectOne<Person, int>(out_person, 1);

    int i = 0;
  }
  MainWindow w;
  w.show();
  return a.exec();
}
