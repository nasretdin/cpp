cmake_minimum_required(VERSION 3.5)

project(qt_sqlite_tmpl LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)



# QtCreator supports the following variables for Android, which are identical to qmake Android variables.
# Check https://doc.qt.io/qt/deployment-android.html for more information.
# They need to be set before the find_package( ...) calls below.

#if(ANDROID)
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
#    if (ANDROID_ABI STREQUAL "armeabi-v7a")
#        set(ANDROID_EXTRA_LIBS
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libcrypto.so
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libssl.so)
#    endif()
#endif()

find_package(QT NAMES Qt6 Qt5 COMPONENTS Widgets Sql Core REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Widgets Core Sql REQUIRED)
#add_subdirectory(lib)

set(PROJECT_SOURCES
        main.cpp
        mainwindow.cpp
        mainwindow.h
        mainwindow.ui
        lib/dbmanager.cpp
        lib/dbmanager.h
        lib/dbmodel.cpp
        lib/dbmodel.h
)


if(${QT_VERSION_MAJOR} GREATER_EQUAL 6)
    qt_add_executable(qt_sqlite_tmpl
        ${PROJECT_SOURCES}
    )
else()
    if(ANDROID)
        add_library(qt_sqlite_tmpl SHARED
            ${PROJECT_SOURCES}
        )
    else()
        add_executable(qt_sqlite_tmpl
            ${PROJECT_SOURCES}
        )
    endif()
endif()

target_link_libraries(qt_sqlite_tmpl PRIVATE Qt${QT_VERSION_MAJOR}::Widgets Qt5::Sql)
#target_link_libraries(DbManager PUBLIC Qt${QT_VERSION_MAJOR}::Core Qt5::Sql)
#target_link_libraries(DBModel PUBLIC Qt${QT_VERSION_MAJOR}::Core Qt5::Sql)
