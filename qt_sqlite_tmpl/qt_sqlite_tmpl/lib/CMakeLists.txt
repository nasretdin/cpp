find_package(QT NAMES Qt6 Qt5 COMPONENTS Sql Core REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Core Sql REQUIRED)

add_library(DbManager
    dbmanager.cpp
    dbmanager.h
    )
target_link_libraries(DbManager PUBLIC Qt${QT_VERSION_MAJOR}::Core Qt5::Sql)

add_library(DBModel
    dbmodel.cpp
    dbmodel.h
    )

#target_link_libraries(DBModel PUBLIC Qt${QT_VERSION_MAJOR}::Core Qt5::Sql)
