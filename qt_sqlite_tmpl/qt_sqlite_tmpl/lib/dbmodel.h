#ifndef DBMODEL_H
#define DBMODEL_H
#include <QList>
#include <QString>
#include <QVariant>
#include <functional>
#include <map>

enum DBTypes { INTEGER, TEXT, BLOB, REAL, NUMERIC };
static std::map<DBTypes, QString> FieldTypes = {{INTEGER, "INTEGER"},
                                                {TEXT, "TEXT"},
                                                {BLOB, "BLOB"},
                                                {REAL, "REAL"},
                                                {NUMERIC, "NUMERIC"}};
enum DBColumnAttributes { AUTOINCREMENT, NOT_NULL, UNIQUE, EMPTY };

static std::map<DBColumnAttributes, QString> ColumnAttributes = {
    {AUTOINCREMENT, "AUTOINCREMENT"},
    {UNIQUE, "UNIQUE"},
    {NOT_NULL, "NOT NULL"},
    {EMPTY, QString()}};

struct Column {
  QString column_name;
  QString column_type;
  QString not_null = QString();
  QString auto_increment = QString();
  QString unique = QString();
  Column(QString columnname, DBTypes columntype,
         DBColumnAttributes notnull = EMPTY,
         DBColumnAttributes autoincrement = EMPTY,
         DBColumnAttributes unique = EMPTY);
};

class DbObjBase {
private:
  QString tablename;
  QList<Column> columns;
  QList<QString> column_names;
  quint16 obj_id;

public:
  virtual QString tableName() = 0;
  virtual QList<Column> columnList() = 0;
  virtual QList<QString> Values() = 0;
  virtual QList<QString> columnNames() = 0;
  virtual QString getObj_id() = 0;
};

class Person final : public DbObjBase {

private:
  quint16 obj_id = 0;
  QString person_name;
  QString person_surename;
  QList<QString> column_names = {"id", "name", "surename"};

  // FIXME if type not INTEGER or NUMBER AUTOINCTEMENT can not be set!!
  // (static_assert?)
  QString tablename = "person";
  QList<Column> columns{{column_names.at(1), TEXT, NOT_NULL, UNIQUE},
                        {column_names.at(2), TEXT, NOT_NULL}};
  void setPerson_name(QString &value);
  void setPerson_surename(QString &value);

public:
  QString tableName();
  QList<Column> columnList();
  Person(){};
  // Person(const quint16 objid);
  Person(QString personname, QString personsurename);
  Person(QList<QVariant> valueslist);
  QList<QString> Values();
  QList<QString> columnNames();
  QString getObj_id();
};


class DevProfile final: public DbObjBase{
    QList<QString> column_names = {"id", "name", "address", "port"};    
    QString tablename = "dev_profiles";
     QList<Column> columns{{column_names.at(1), TEXT, NOT_NULL, UNIQUE},
                        {column_names.at(2), TEXT, NOT_NULL},
                        {column_names.at(3), TEXT, NOT_NULL}};
    
}

#endif // DBMODEL_H
