#include "tablebase.h"

Person::Person(QString personname, QString personsurename): personName(personname), personSureName(personsurename)
{

}

QString Person::tableName()
{
  return this->tablename;
}

QList<Column> Person::columnList()
{
  return this->columns;
}

Column::Column(QString columnname, DBTypes columntype,
               DBColumnAttributes notnull, DBColumnAttributes autoincrement, DBColumnAttributes unique) :
  columnName(columnname),
  columnType(FieldTypes[columntype]),
  notNull(ColumnAttributes[notnull]),
  autoIncrement(ColumnAttributes[autoincrement]),
  Unique(ColumnAttributes[unique])

{

}
