#ifndef TABLEBASE_H
#define TABLEBASE_H

#include <QtCore/QString>
#include <QList>
#include <map>

enum DBTypes { INTEGER, TEXT, BLOB, REAL, NUMERIC };
static std::map<DBTypes, QString> FieldTypes = {{INTEGER, "INTEGER"},
                                              {TEXT, "TEXT"},
                                              {BLOB, "BLOB"},
                                              {REAL, "REAL"},
                                              {NUMERIC, "NUMERIC"}
                                             };
enum DBColumnAttributes {AUTOINCREMENT, NOT_NULL, UNIQUE, EMPTY};

static std::map<DBColumnAttributes, QString> ColumnAttributes = {{AUTOINCREMENT, "AUTOINCREMENT"},
                                                                 {UNIQUE, "UNIQUE"},
                                                                 {NOT_NULL, "NOT NULL"},
                                                                 {EMPTY, QString()}
                                                                };

struct Column{
  QString columnName;
  QString columnType;
  QString notNull = QString();
  QString autoIncrement= QString();
  QString Unique = QString();
  Column(QString columnname, DBTypes columntype,
         DBColumnAttributes notnull = EMPTY, DBColumnAttributes autoincrement = EMPTY, DBColumnAttributes unique = EMPTY);
};


class TableBase
{
private:
  const QString tablename;
  const QList<Column> columns;

public:
  virtual QString tableName() = 0;
  virtual QList<Column> columnList() = 0;
};

class Person : public TableBase{

private:
  const QString tablename = "person";
  const QList<Column> columns {{"name", TEXT, NOT_NULL, AUTOINCREMENT },
                               {"surename", TEXT,NOT_NULL}};
  const QString personName;
  const QString personSureName;


public:
  QString tableName();
  QList<Column> columnList();
  Person(QString personname, QString personsurename);
};

#endif // TABLEBASE_H
