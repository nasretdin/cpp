#include "dbmanager.h"
#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <qlist.h>

DbManager::DbManager(const QString &path) {
    m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db.setDatabaseName(path);

    if (!m_db.open()) {
        qDebug() << "Error: connection with database fail";
    } else {
        qDebug() << "Database: connection ok";
    }
}

DbManager::~DbManager() {
    if (m_db.isOpen()) {
        m_db.close();
    }
}

bool DbManager::isOpen() const { return m_db.isOpen(); }

bool DbManager::removePerson(const QString &name) {
    bool success = false;

    if (personExists(name)) {
        QSqlQuery queryDelete;
        queryDelete.prepare("DELETE FROM people WHERE name = (:name)");
        queryDelete.bindValue(":name", name);
        success = queryDelete.exec();

        if (!success) {
            qDebug() << "remove person failed: " << queryDelete.lastError();
        }
    } else {
        qDebug() << "remove person failed: person doesnt exist";
    }

    return success;
}

bool DbManager::personExists(const QString &name) const {
    bool exists = false;

    QSqlQuery checkQuery;
    checkQuery.prepare("SELECT name FROM people WHERE name = (:name)");
    checkQuery.bindValue(":name", name);

    if (checkQuery.exec()) {
        if (checkQuery.next()) {
            exists = true;
        }
    } else {
        qDebug() << "person exists failed: " << checkQuery.lastError();
    }

    return exists;
}

bool DbManager::makeTable(DbObjBase &table) {
    bool success = false;

    QSqlQuery query;
    QString tblnm = table.tableName();
    QList<Column> coumns = table.columnList();

    QString request =
            "CREATE TABLE " + table.tableName() + "(id INTEGER PRIMARY KEY, ";

    for (int i = 0; i < table.columnList().count(); ++i) {

        request += table.columnList().at(i).column_name + " " +
                table.columnList().at(i).column_type + " " +
                table.columnList().at(i).unique + " " +
                table.columnList().at(i).auto_increment + " " +
                table.columnList().at(i).not_null;
        if (i != table.columnList().count() - 1) {
            request += ", ";
        }
    }

    request += ");";
    query.prepare(request);
    success = true;

    if (!query.exec()) {
        qDebug() << "Couldn't create the table '" << table.tableName()
                 << "Request: " << request;
        qDebug() << query.lastError().text();
        success = false;
    }

    return success;
}

bool DbManager::insert(DbObjBase &tableObject) {
    bool success = false;

    if (tableObject.tableName() != "") {
        if (m_db.transaction()) {
            QSqlQuery queryAdd(m_db);
            QString request = "INSERT INTO " + tableObject.tableName() + " ";
            QString values = "(";
            QString column_names = "(";

            for (int i = 0; i < tableObject.Values().count(); ++i) {
                values += "'" + tableObject.Values().at(i) + "', ";
                column_names += tableObject.columnNames().at(i + 1) + ", ";
                // id accepted automatically
                //        if (i < tableObject.Values().count()) {
                //          values += ", ";
                //          column_names += ", ";
                //        } else {
                //          values += ")";
                //          column_names += ") ";
                //        }
            }

            values.remove(values.lastIndexOf(","), 1);
            values.replace(values.length() - 1, 1, ")");
            column_names.remove(column_names.lastIndexOf(","), 1);
            column_names.replace(column_names.length() - 1, 1, ")");

            request += column_names + " VALUES " + values + ";";
            queryAdd.prepare(request);
            success = true;
            if (!queryAdd.exec()) {
                qDebug() << request << " Last error: " << queryAdd.lastError().text();
            }

            if (!m_db.commit()) {
                qDebug() << "Failed to commit: " << m_db.lastError().text();
                m_db.rollback();
            }
        } else {
            qDebug() << "Failed to start transaction mode.";
            success = false;
        }

    } else {
        qDebug() << "Insert failed: tablename cannot be empty.";
    }
    return success;
}
