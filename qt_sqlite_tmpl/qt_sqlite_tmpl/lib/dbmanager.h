#ifndef DBMANAGER_H
#define DBMANAGER_H

#include "dbmodel.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>

/**
 * \class DbManager
 *
 * \brief SQL Database Manager class
 *
 * DbManager sets up the connection with SQL database
 * and performs some basics queries. The class requires
 * existing SQL database. You can create it with sqlite:
 * 1. $ sqlite3 people.db
 * 2. sqilte> CREATE TABLE people(ids integer primary key, name text);
 * 3. sqlite> .quit
 */

class DbManager {
public:
    /**
   * @brief Constructor
   *
   * Constructor sets up connection with db and opens it
   * @param path - absolute path to db file
   */
    DbManager(const QString &path);

    /**
   * @brief Destructor
   *
   * Close the db connection
   */
    ~DbManager();

    bool isOpen() const;

    /**
   * @brief database
   * @return Current database object
   */
    const QSqlDatabase &database();

    /**
   * @brief Remove person data from db
   * @param name - name of person to remove.
   * @return true - person removed successfully, false - person not removed
   */
    bool removePerson(const QString &name);

    /**
   * @brief Check if person of name "name" exists in db
   * @param name - name of person to check.
   * @return true - person exists, false - person does not exist
   */
    bool personExists(const QString &name) const;

    /**
   * @brief Create table in DB
   * @param table - instance of TableBase child
   * @return true - table added successfully, false - table not added
   */
    bool makeTable(DbObjBase &table);

    /**
   * @brief Insert new object in database table if it doesn't already exist
   * @return true - insert request if successfully, else - false
   */
    bool insert(DbObjBase &tableObject);

    /**
   * @brief Select TableBase child object from DB by id
   * @param tableObject - empty TableBase child reference with id value
   */

    template <class T, class I> void selectOne(T &tableObject, int id) {
        if (tableObject.tableName() != "") {
            QSqlQuery querySelect(m_db);
            QString colunmnames = "";
            for (auto columnname : tableObject.columnNames()) {
                if (columnname != tableObject.columnNames().last()) {
                    colunmnames += columnname + ", ";
                } else {
                    colunmnames += columnname;
                }
            }

            QString request = "SELECT " + colunmnames + " FROM " +
                    tableObject.tableName() +
                    " WHERE id = " + QString::number(id) + ";";
            querySelect.prepare(request);
            if (!querySelect.exec()) {
                qDebug() << request
                         << " Last error: " << querySelect.lastError().text();
                return;
            }
            QList<QVariant> objectdata = {};

            while (querySelect.next()) {
                for (auto column : tableObject.columnNames()) {
                    QString strValue = querySelect.value(column).toString();
                    objectdata.append(strValue);
                }
            }

            T newobj = T(objectdata);
            std::swap(tableObject, newobj);

        } else {
            qDebug() << "Select failed: tablename cannot be empty.";
        }
    }

private:
    QSqlDatabase m_db;
};

#endif // DBMANAGER_H
