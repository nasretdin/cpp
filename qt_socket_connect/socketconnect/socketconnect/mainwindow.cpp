#include "mainwindow.h"
#include "./ui_mainwindow.h"

static int counter = 1;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(this, SIGNAL(addText()),this, SLOT(incrementSpin()));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    ui->textEdit->insertPlainText(" test test ");
    emit addText();

}

void MainWindow::incrementSpin()
{
    ui->spinBox->setValue(counter++);
}
