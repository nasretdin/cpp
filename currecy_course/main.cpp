//Напишите программу обмена валют: программа запрашивает текущий курс доллара, например, к рублю, и
// количество единиц (рублей) для конвертации и выводит на консоль сконвертированную сумму в долларах.


#include <iostream>

int main() {
  std::cout << "Enter Rouble/dollar course: ";
  double course = 0.0;

  std::cin >> course;
  if (course <= 0){
    std::cout << "Invalid value\n";
    return 1;
  }

  std::cout << "Enter roubles sum: ";
  uint16_t roubles = 0;
  std::cin >> roubles;
  if (roubles <= 0){
    std::cout << "Invalid value\n";
    return 1;
  }
  double dollars = roubles / course;

  std::cout <<"The " << roubles << "roubles is: " << dollars << "$" << std::endl;
  return 0;
}
