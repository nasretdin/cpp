#include <iostream>
#include <fstream>
#include <string>


const char* InPrefix = ">> ";
const char* OutPrefix = "<< ";


template <typename T>
void print_str_to_stdout(const T& text) {
	std::cout << text << std::endl;
}
template <typename T>
void print_str(std::ostream& out, const T& text) {
	out << text << std::endl;
}


void exercise1(std::ostream& out) {
	
	/*Напишите программу на C++, которая отобразит ваше имя и адрес (можете
	указать фиктивные данные).*/
	print_str(out, "Name: Ira Sobolevskaya");
	print_str(out, "Address: Bozhedomka dom 7.");
}

void exercise2() {

	/*Напишите программу на C++, которая выдает запрос на ввод расстояния в фарлон-
	гах и преобразует его в ярды. (Один фарлонг равен 220 ярдам, или 201168 м.)*/
	print_str_to_stdout("Enter distance in farlong");
	std::cout << InPrefix << "Enter distance in farlong:\n";

	int farlond_distance = 0;
	std::cin >> farlond_distance;
	const int yards_in_farlong = 220;
	int result_yards = farlond_distance * yards_in_farlong;

	std::cout << OutPrefix << "Distance in yards: " << result_yards << "\n";
}

namespace exercise3 {
	/*Напишите программу на C++, которая использует три определяемых
	пользователем функции (включая main ()) и генерирует следующий вывод:
	Three blind mice
	Three blind mice
	See how they run
	See how they run
	Одна функция, вызываемая два раза, должна генерировать первые две строки,
	а другая, также вызываемая два раза — оставшиеся строки.*/

	void print_first_phrase() {
		std::cout << "Three blind mice" << std::endl;
	}

	void print_second_phrase() {
		std::cout << "See how they run" << std::endl;
	}

	void main() {
		for (int i = 0; i <= 1; i++) {
			print_first_phrase();
		}
		for (int j = 0; j <= 1; j++) {
			print_second_phrase();
		}
	}

}

void exercise4() {
	/*Напишите программу, которая предлагает пользователю ввести свой возраст.
	Затем программа должна отобразить возраст в месяцах:
	Enter your age: 29
	Your age in months is 348.*/
	std::cout << "Enter your age: ";
	int user_age = 0;
	std::cin >> user_age;

	int age_in_month = user_age * 12;
	std::string answer = "Your age in months is: " + std::to_string(age_in_month);
	print_str_to_stdout(answer);
}


void exercise5() {
	/*Напишите программу, в которой функция main() вызывает определяемую
	пользователем функцию, принимающую в качестве аргумента значение
	температуры по Цельсию и возвращающую эквивалентное значение температуры по
	Фаренгейту. Программа должна выдать запрос на ввод значения по Цельсию и
	отобразить следующий результат:
	Please enter a Celsius value: 20
	20 degrees Celsius is 68 degrees Fahrenheit.
	Вот формула для этого преобразования:
	Температура в градусах по Фаренгейту =
	1,8 * Температура в градусах по Цельсию + 32*/

	std::cout << InPrefix << "Please enter a Celsius value: ";
	int celsuis_degiees = 0;
	std::cin >> celsuis_degiees;
	int farengate_degrees = 1.8 * celsuis_degiees + 32;
	std::string answer = std::to_string(celsuis_degiees) + " degrees Celsius is " + std::to_string(farengate_degrees) + " degrees Fahrenheit.";
	print_str_to_stdout(answer);
}

void exercise6() {
	/*Напишите программу, в которой функция main () вызывает определяемую
пользователем функцию, принимающую в качестве аргумента расстояние в световых
годах и возвращающую расстояние в астрономических единицах. Программа
должна выдать запрос на ввод значения в световых годах и отобразить
следующий результат:
Enter the number of light years: 4.2
4.2 light years = 265608 astronomical units.
Астрономическая единица равна среднему расстоянию Земли от Солнца (около
150 000 000 км, или 93 000 000 миль), а световой год соответствует расстоянию,
пройденному лучом света за один земной год (примерно 10 триллионов
километров, или 6 триллионов миль). (Ближайшая звезда после Солнца находится
на расстоянии 4,2 световых года.) Используйте тип double (как в листинге 2.4)
и следующий коэффициент преобразования:
1 световой год = 63 240 астрономических единиц*/
	const int astro_units = 63240;
	std::cout << "Enter the number of light years: ";
	double light_years = 0.0;
	std::cin >> light_years;
	double const astronomical_units = light_years * astro_units;
	std::string answer = std::to_string(light_years) + " light years = " + std::to_string(astronomical_units) + " astronomical units.";
	print_str_to_stdout(answer);
}

void exercise7() {
	/*Напишите программу, которая выдает запрос на ввод значений часов и минут.
	Функция main () должна передать эти два значения функции, имеющей тип
	void, которая отобразит эти два значения в следующем виде:
	Enter the number of hours: 9
	Enter the number of minutes: 28
	Time: 9:28*/

	std::cout << InPrefix << "Enter the number of hours: ";
	int hours = 0;
	std::cin >> hours;

	std::cout << "Enter the number of minutes: ";
	int minutes = 0;
	std::cin >> minutes;
    std::cout << OutPrefix <<  "Time: " << hours << ":" << minutes << std::endl;

	std::string answer = "Time: " + std::to_string(hours) + ":" + std::to_string(minutes);
	print_str_to_stdout(answer);
}

int main() {
	exercise1(std::cout);
	std::ofstream f("MyText.txt");
	exercise1(f);
	// exercise3::main();
	// exercise4();
	// exercise5();
	// exercise6();
	// exercise7();
}