﻿// inheritance.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <list>

class Base {
protected:
	int F = 0;
public:
	virtual void printF()const {
		std::cout << "Base f = " << F <<"\n";
	}
};

class A: public Base {
public:
	int F=5;
	
	void printF()const override {
		std::cout << "Base f = " << F << "\n";
	}
	void Sub(int val){
		F-=val;
		Base::F -= val;
	}
};

class C {
protected:
	class D {

	};
public:
	Base B;
	Base B2;
	D d;
	int F = 5;

	void printF()const {
		std::cout << "Base f = " << F << "\n";
	}
};


struct Animal {
	virtual void Say()=0;
};

struct Cat:public Animal {
	Cat(int age): Age(age){}
	virtual void Say() {
		std::cout << "Meu, I`m "<< Age <<" years old\n";
	}

	int Age;
};

struct Dog :public Animal {
	virtual void Say() {
		std::cout << "Gav\n";
	}
};

struct Fish :public Animal {
	virtual void Say() {
		std::cout << "...\n";
	}

};


void Foo(const Base& b) {
	b.printF();
	
}

int main() {
	Base b;
//	static_assert(sizeof(b) == 4,"");
	b.printF();
	A a;
//	static_assert(sizeof(a) == 8, "");
	a.printF();
	//a.
	Foo(a);
	Foo(b);
	//std::cout << "Hello World!\n";

////	Dog d1;
//	cin >> str;

	std::list<Animal*> Zoo;
	Zoo.push_back(new Cat(1));
	Zoo.push_back(new Dog);
//	Zoo.push_back(&d1);
	Zoo.push_back(new Fish);
	Zoo.push_back(new Cat(3));
	Zoo.push_back(new Fish);

	for (auto a : Zoo) {
		a->Say();
	}

	for (auto a : Zoo) {
		delete a;
	}

}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
