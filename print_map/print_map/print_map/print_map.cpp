﻿#include <iostream>
#include <list>
#include <map>
#include <string>

using namespace std;

struct MapItemFields {
  std::string name;
  std::string surname;
  bool is_crazy;
};

int main() {

  std::map<std::string, MapItemFields> helpDescrs;
  MapItemFields firstItem;
  firstItem.name = "Ivan";
  firstItem.surname = "Petrovich";
  firstItem.is_crazy = true;

  MapItemFields secondItem;
  secondItem.name = "Ilya";
  secondItem.surname = "Ilich";
  secondItem.is_crazy = false;

  helpDescrs["first"] = firstItem;
  helpDescrs["second"] = secondItem;

  std::cout << helpDescrs["first"].name << std::endl;
  std::cout << helpDescrs["second"].name << std::endl;
}
