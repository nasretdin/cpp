/*
 * Создайте программу, которая переводит метры в киллометры. Например, пользователь вводит 2345 метров, а программа
 * в ответ отображает 2 километра и 345 метров.*/
#include <iostream>

int main() {
  std::cout << "Enter meters.\n";
  uint16_t meters_in = 0;
  std::cin >> meters_in;

  uint16_t meters = meters_in % 1000;
  uint16_t kilometers = meters_in / 1000;

  std::cout << kilometers << " kilometers " << meters << " meters." << std::endl;
  return 0;
}
