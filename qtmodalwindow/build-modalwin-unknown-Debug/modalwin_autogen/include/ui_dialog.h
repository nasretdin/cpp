/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QLineEdit *lineEditProfileName;
    QLineEdit *lineEditIPAddress;
    QLineEdit *lineEditPortNum;
    QPushButton *pushButtonOK;
    QPushButton *pushButton;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QString::fromUtf8("Dialog"));
        Dialog->resize(400, 300);
        lineEditProfileName = new QLineEdit(Dialog);
        lineEditProfileName->setObjectName(QString::fromUtf8("lineEditProfileName"));
        lineEditProfileName->setGeometry(QRect(90, 60, 231, 32));
        lineEditIPAddress = new QLineEdit(Dialog);
        lineEditIPAddress->setObjectName(QString::fromUtf8("lineEditIPAddress"));
        lineEditIPAddress->setGeometry(QRect(90, 110, 231, 32));
        lineEditPortNum = new QLineEdit(Dialog);
        lineEditPortNum->setObjectName(QString::fromUtf8("lineEditPortNum"));
        lineEditPortNum->setGeometry(QRect(90, 160, 231, 32));
        pushButtonOK = new QPushButton(Dialog);
        pushButtonOK->setObjectName(QString::fromUtf8("pushButtonOK"));
        pushButtonOK->setGeometry(QRect(50, 240, 88, 34));
        pushButton = new QPushButton(Dialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(230, 240, 88, 34));

        retranslateUi(Dialog);

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QCoreApplication::translate("Dialog", "Dialog", nullptr));
        pushButtonOK->setText(QCoreApplication::translate("Dialog", "OK", nullptr));
        pushButton->setText(QCoreApplication::translate("Dialog", "\320\236\321\202\320\274\320\265\320\275\320\260", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
