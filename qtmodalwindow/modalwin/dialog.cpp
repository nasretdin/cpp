#include "dialog.h"
#include "ui_dialog.h"


ProfileDialog::ProfileDialog(QDialog *parent) :
  QDialog(parent),
  ui(new Ui::Dialog)
{
  ui->setupUi(this);
}


ProfileDialog::~ProfileDialog()
{
  delete ui;

}


void ProfileDialog::on_pushButtonOK_clicked()
{
  this->profileName = ui->lineEditProfileName->text();
  this->profileAddress = ui->lineEditIPAddress->text();
  this->profilePort = ui->lineEditPortNum->text();

  this->setResult(QDialog::Accepted);
  this->close();

}

void ProfileDialog::on_pushButton_clicked()
{
  this->setResult(QDialog::Rejected);
  this->close();
}

void ProfileDialog::setFieldsValues(const QString& profileName){
  ui->lineEditProfileName->setText(profileName);

}

void ProfileDialog::on_Dialog_finished(int result)
{

}
