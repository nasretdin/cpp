#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QInputDialog>

namespace Ui {
  class Dialog;
}

class ProfileDialog : public QDialog
{
  Q_OBJECT

public:
  explicit ProfileDialog(QDialog *parent = nullptr);
  ~ProfileDialog();
  QString profileName = "";
  QString profileAddress = "";
  QString profilePort = "";
  void setFieldsValues(const QString& profileName);

private slots:
  void on_pushButtonOK_clicked();

  void on_pushButton_clicked();

  void on_Dialog_finished(int result);



private:
  Ui::Dialog *ui;
};

#endif // DIALOG_H
