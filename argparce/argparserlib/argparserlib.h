#pragma once
#include <string>
#include <functional>
#include <list>
#include<map>


#define WIN32_LEAN_AND_MEAN


using ValidatorFn = bool(*)(int);

class Argument {
public:
	Argument() = default;
	Argument(const std::string key, const std::string help, int& val) : Key(key), HelpStr(help), Val(&val) {};

	Argument* Required() {
		IsRequired = true;
		return this;
	}
	Argument* Validate(ValidatorFn fn) {
		Validator = fn;
		return this;
	}
	bool Matches(const char* arg) { return Key == arg; }
	bool Accept(const char* val) {
		int myVal = 0;//LexicalCast<int>     val -> to int
		bool valid = Validator(myVal);
		if (valid)
			*Val = myVal;
		return valid;
	}
	//private:
	std::string Key;
	std::string HelpStr;
	int* Val;
	bool IsRequired = false;
	ValidatorFn Validator = [](int)->bool {return true; };
};


class ArgParser {
public:
	ArgParser();
	Argument* addArgument(const std::string key, const std::string help, int& val);

	void parseArgs(int argc, char *cmd_args[]);
	void printHelp(const std::string agrkey);

private:
	bool runValidate(const char value, const bool(*func)(char));
	std::map<std::string, Argument> arguments_map = {};
	std::list<Argument> argument_list = {};
	int PrintHelpRequired = 0;
	std::map<std::string, const char*> mapReceivedArgs(int argc, char* argv[]);
};