﻿#include "argparserlib.h"
#include <iostream>
#include <string>
#include <exception>


ArgParser::ArgParser() {
	addArgument("-h", "Print help", PrintHelpRequired);
}
bool ArgParser::runValidate(const char value, const bool(*func)(char)) {
	return func(value);
}

Argument* ArgParser::addArgument(const std::string key, const std::string help, int& val) {
	arguments_map[key] = Argument(key, help, val);
	argument_list.push_back(Argument(key, help, val));
	return &arguments_map[key];
}

std::map<std::string, const char*> ArgParser::mapReceivedArgs(int argc, char* argv[]) {
	std::map<std::string, const char*> recived_args_map;
	std::string arg = "";
	std::string dash = "-";
	const char* empty = "";

	int i = 1;
	while (i < argc) {
		arg.assign(argv[i]);

		if (arg.compare(0, 1, dash) == 0) {
			std::string key = argv[i];
			if (arg.compare(0, 2, dash) != 0){
			recived_args_map[key] = argv[i + 1];
			i++;
			}else{
				recived_args_map[key] = empty;
			}
		} else {
			//Invalid arg; no dash char found
			printHelp("");
		}
		i++;
	}
	return recived_args_map;
}

void ArgParser::parseArgs(int argc, char* argv[]) {
	std::map received_args_map = mapReceivedArgs(argc, argv);
	for (auto const arg : received_args_map){
		auto found = arguments_map.find(arg.first);
		if (found != arguments_map.end()) {
			if (!found->second.Accept(arg.second)) {
				///  prin help for this arg ...
			}
		} else {
			///  prin help for all args ...
			break;
		}
	
	}

	for (int count = 1; count < argc; count++) {
//		std::cout << "  argv[" << count << "]   " << argv[count] << "\n";
	
		auto found = arguments_map.find(argv[count]);
		if (found != arguments_map.end()) {
			if (!found->second.Accept(argv[count + 1])) {
				///  prin help for this arg ...
			}
		} else {
			///  prin help for all args ...
			break;
		}

		//this->validate();
	}
}

void ArgParser::printHelp(const std::string argkey) {
	if (argkey == " ") {
		for (const auto& argument : this->arguments_map) {
			std::cout << argument.first << argument.second.HelpStr << std::endl;
		}
	} else {
		std::cout << this->arguments_map[argkey].HelpStr << std::endl;
	}
}
