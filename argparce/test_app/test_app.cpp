﻿#include <iostream>
#include "argparserlib.h"
#include <algorithm>


bool multiply() {
	return true;
}

bool add(int x) {
	return x < 10 && x >-10;
}


#include <string_view> // If C++17 can't be used, use the GSL string_view or implement your own struct

namespace Reflection {

	template <typename T> std::string_view TypeName();

	template <> std::string_view TypeName<void>() { return "void"; }

	namespace detail {
		/** \brief
		for G++:     constexpr std::string_view Reflection::detail::ProbeFunc() [with T = void; std::string_view = std::basic_string_view<char>]"
		for MSVC:    class std::basic_string_view<char,struct std::char_traits<char> > __cdecl Reflection::detail::ProbeFunc<void>(void)
					 __PRETTY_FUNCTION__ assumed to be defined as __FUNCSIG__ in build system for Microsoft compiler
		*/
		template <typename T>  std::string_view ProbeFunc() { return __FUNCSIG__; }

		std::size_t ProbeFunc_PrefixLen() {
			return ProbeFunc<void>().find(TypeName<void>());
		}

		std::size_t ProbeFunc_SuffixLen() {
			return ProbeFunc<void>().length() - ProbeFunc_PrefixLen() - TypeName<void>().length();
		}

	} // namespace detail

	template <typename T>
	std::string_view TypeName() {
		auto funcName = detail::ProbeFunc<T>();
		auto prefixLen = detail::ProbeFunc_PrefixLen();
		auto suffixLen = detail::ProbeFunc_SuffixLen();
		auto typeNameLen = funcName.length() - prefixLen - suffixLen;
		return funcName.substr(prefixLen, typeNameLen);
	}

} //namespace Reflection


template <typename T>
constexpr auto TestFuncName(const T&) {
	auto signature = Reflection::TypeName<T>();
	std::cout << signature << std::endl;
	return signature;
}

int main(int argc, char* argv[]) {




	//int arr_len = sizeof(arr) / sizeof(arr[0]);
	//while (true) {
	//	auto qqq = &arr;
	//	int* zero = &arr[0];
	//	int* zero1 = &arr[1]; // address (pointer)
	//	int www = *zero1; // value
	//	int* zero2 = &arr[2];
	//	int i = 0;
	//}
	//TestFuncName(8);
	//char c = 0;
	//TestFuncName(c);

	ArgParser argparser;
//	TestFuncName(argparser);

	int cArg=9;
	int dArg = 0;

	argparser.addArgument("-c", "Counter.", cArg)
		->Validate(add);
	argparser.addArgument("-d", "Descr.", dArg)
		->Required()
		->Validate(add);
	argparser.parseArgs(argc, argv);

// int d = argparser.Get("-c");
	return 0;

}

