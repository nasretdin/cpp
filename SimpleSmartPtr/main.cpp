#include <iostream>


template<typename T>
struct SmartPtr{
    T* ptr;
    SmartPtr(T* p){ 
        ptr = p; 
        std::cout << "SP CTOR" << std::endl; 
        std::cout << "ptr value: " << *ptr << std::endl;
        }
    SmartPtr(SmartPtr& ) = delete;
    SmartPtr& operator=(const SmartPtr) = delete;
    ~SmartPtr(){
        std::cout << "SP DTOR" << std::endl;
        delete (ptr);
    }
    T& operator*(){
        return *ptr;
    }
};


class Worker{
public:
    Worker(int x) : counter(new int(x)){
        std::cout << "worker ctor" << std::endl;
        std::cout << "counter value: " << *counter.ptr << std::endl;
    }
    ~Worker(){
        std::cout << "worker dtor" << std::endl;
    }
    void ptintArg(){
        std::cout << counter.ptr;
    }


private:
    SmartPtr<int> counter;
};


int main(){
    Worker worker(8);
    return 0;
}