import ctypes as ct


class PyMyStruct(ct.Structure):
    _fields_ = [("f1", ct.c_int), ("f2", ct.c_int)]


if __name__ == '__main__':
    py_mystruct = PyMyStruct()
    py_mystruct.f1 = 12
    py_mystruct.f2 = 124

    # _sharedlib = ct.CDLL("./libshared.so")
    _sharedlib = ct.cdll.LoadLibrary("./libshared.so")

    # _sharedlib.func_ret_str.restype = ct.c_char_p
    # _sharedlib.func_ret_str.argtypes = [ct.POINTER(ct.c_char), ]
    # print('ret func_ret_str: ', _sharedlib.func_ret_str('Hello!'.encode('utf-8')).decode("utf-8"))

    _sharedlib.get_info.restype = ct.c_int
    _sharedlib.get_info.argstype = [ct.POINTER(PyMyStruct)]
    result = _sharedlib.get_info(ct.byref(py_mystruct))
    print(result)
