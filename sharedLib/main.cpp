#include <iostream>
#include <dlfcn.h>
#include<cstring>
#include <vector>
#include <memory_resource>
#include "shared.h"

/*
std::pmr::vector<std::string> get_info(){

    std::string buffer[256] = {}; // a small buffer on the stack
    std::pmr::monotonic_buffer_resource pool{std::data(buffer), std::size(buffer)};
    std::pmr::vector<std::string> vec{ &pool };
    vec.reserve(5);

    for (int i = 0; i < 5; ++i){
        const std::string tmp = "iter: " + std::to_string(i);
        vec.emplace_back(tmp);
    }

    return vec;
}

const char* main_proc(int argc, char* argv[]){
    std::pmr::vector<std::string> info_v = get_info();
    uint64_t char_count = 0;
    for(unsigned i = 0; i <= info_v.capacity(); i++){
        char_count += info_v[i].length() + 1;
    }

    const uint64_t char_count_c = char_count;
    std::string info_s[char_count_c];

    // std::string info_s;
    //info_s.reserve(char_count);
    for(unsigned i = 0; i < info_v.size(); ++i){
    //for(auto it = begin (info_v); it != end (info_v); ++it){
    //    info_s.reserve(1);
        info_s[i] = info_v[i];
    }

    int tmp_i = argc;
    const char* info_c = info_s->c_str();
    return info_c;
}

int main(){

//    std::string param0_s = "argvs";
//    std::string param1_s = "show";
//    char *param0 = new char[param0_s.length() + 1];
//    char *param1 = new char[param1_s.length() + 1];
//    strncpy(param0, param0_s.c_str(), sizeof (param0_s));
//    strncpy(param1, param1_s.c_str(), sizeof (param0_s));
//    char *param[] = {param0, param1};
    const char* argv[] = {
        "programm", "show"
    };


    const char* rc = main_proc(2, argv);
    return 0;
}

*/



int main() {

    //typedef char* main_proc_t(int, const char *[]);
    typedef int get_info_t(MyStruct *mystruct) ;

    void *sharedlib = dlopen("./libshared.so", RTLD_NOW | RTLD_GLOBAL);

    if (!sharedlib) {
     std::cerr << "Cannot open library: " << dlerror() << '\n';
     return 1;
     }
    get_info_t *get_info = (get_info_t*) dlsym(sharedlib, "get_info");

     // main_proc_t *main_proc = (main_proc_t*) dlsym(sharedlib, "main_proc");

     const char *dlsym_error = dlerror();
     if (dlsym_error) {
     std::cerr << "Cannot load symbol 'hello': " << dlsym_error <<   '\n';
     dlclose(sharedlib);
     return 1;
     }

     /*const char* argv[] = {
         "programm_name", "show"
     };
     int argc = sizeof (argv);


    const char* rc = main_proc(argc, argv);
    std::cout << rc << std::endl;
    */

     MyStruct newmystruct = {5, 3};
    int ret = get_info(&newmystruct);
    std::cout << ret << "\n";

    dlclose(sharedlib);
    return 0;
}

