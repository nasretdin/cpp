#pragma once
#include <iostream>

// extern: https://docs.microsoft.com/en-us/cpp/cpp/extern-cpp?view=vs-2019
// why extern "C" : https://stackoverflow.com/a/42183390/2790983

struct MyStruct{
    int f1 = 0;
    int f2 = 1;
};

extern "C" {
    // function declaration
    int get_info(MyStruct *mystruct);
    char* func_ret_str(char *val);
}
