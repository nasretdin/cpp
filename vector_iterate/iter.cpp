//
// Created by ray on 24.04.24.
//
#include "iter.h"

void iterate(std::vector<std::uint16_t>& vector_){
  for (int i = 0; i < vector_.size(); ++i) {
    std::cout << vector_[i] << " | ";
  }
  std::cout << '\n';

  for (auto myvitem : vector_) {
    std::cout << myvitem << " | ";
  }
  std::cout << '\n';

  for (auto it = vector_.begin(); it != vector_.end(); ++it)
    std::cout << *it << " | ";
  std::cout << '\n';

  for (std::uint16_t &it : vector_)
    std::cout << it << " | ";
  std::cout << '\n';
}
