#include <vector>
#include <cstdint>
#include "iter.h"

int main() {

  std::vector<std::uint16_t> myvectr{1, 4, 66, 24, 54, 69, 100};
  iterate(myvectr);
  return 0;
}
