﻿#include <iostream>

using check_arr = int[9];

int getSingleMember(check_arr& in_arr) {
	for (int& num : in_arr) {
		int counter = 0;
		for (int& next : in_arr) {
			if (num == next) {
				counter++;
			}
		}
		if (counter == 1) {
			std::cout << "\nnum: " << num << std::endl;
			std::cout << "counetr: " << counter << std::endl;
			return num;
		}
	}
}


int getSingleMember_x(check_arr& in_arr) {
	int single_elem = 0;
	for (int& num : in_arr) {
		single_elem = single_elem xor num;
	}
	return single_elem;
}

// just another form argument definition
int getSingleMember_t(int (&in_arr)[9]){  
	return getSingleMember_x(in_arr);
}


int main() {

	int arr[9] = { 1, 2, 3,  2, 7, 7, 5, 1,  5 };
	int single = getSingleMember_t(arr);
	std::cout << single << std::endl;
}

