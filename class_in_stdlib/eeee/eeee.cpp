﻿// eeee.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "framework.h"
#include <stdexcept>


//Constructor/Destructor
//Reference
//Exception

using namespace std;

class A {
public:
	int X=9;
	char chr = 'r';
	A(): X(0), chr('t') {
		std::cout << "Default ctor\n";
	}

	explicit A(int x): X(x)  {
		std::cout << "ctor(int)!\n";
	}

	 A(int x, char c) : X(x) ,chr(c) {
		std::cout << "ctor(int)!\n";
	}


	A(const A& other): X(other.X), chr(other.chr) {
		std::cout << "Copy ctor and other.chr" << other.chr << std::endl;
	}
	A& operator = (const A& other) {

	}

	~A() {
		std::cout << "dtor "<< X << ":"<< chr <<"\n";
	}
};

void Bar(A arg) {
	std::cout << "Bar: " << arg.X << std::endl;
}



void Bar(A* arg) {
	std::cout << "Bar: " << arg->X << std::endl;
}


struct File {
private:
	FILE* F;
public:
	File(const char* name) {
		auto err = fopen_s(&F, name,"r");
		if (err)
			throw err;

	}
	File(const File& other) = delete;
	File& operator = (const File& other) = delete;

	~File() {
		fclose(F);
	}
	void Read(void* buff, size_t bufSize) {
		fread(buff, 1, bufSize, F);
	}
};

struct Str{
	char* Data;
	Str(const char* s) {
		Data = new char[strlen(s) + 1];
		strcpy_s(Data, strlen(s) + 1, s);
	}

	Str(const Str& other): Data(nullptr) {
		Data = new char[strlen(other.Data) + 1];
		strcpy_s(Data, strlen(other.Data) + 1, other.Data);
		std::cout << "Copy ctor and other.chr" << other.Data << std::endl;
	}
	Str& operator = (const Str& other){
		if (this == &other)
			return *this;
		delete[] Data;
		Data = new char[strlen(other.Data) + 1];
		strcpy_s(Data, strlen(other.Data) + 1, other.Data);
		return *this;
	}

	~Str() {
		delete[] Data;
	}
};

void FooBax(Str s) {
	File f("C:\\tmp\\json.cpp");
	char buff[100];
	f.Read(buff, sizeof(buff));
	//...
}

int main(){
	
	try{
		A a;
		A b(4, 'y');
		A c ( 9, 'z' );
		Bar(b);
		int intB = 99;
		Bar({ 10,'E' });
		Bar(&b);
		Foo(b.X);
		Foo(a.X);

		Str s2 ="Hello Bax";
		Str s3{""};
		// s3 = s3;
		
		for (;;) {
		//	std::string name = GetFileNameFromUser();
			FooBax(s2);
		}
		
		FooBax(s2);
		///...
		//s2.
	}
	catch (errno_t err) {
		std::cerr << "errno= " << err;
	}
	catch (...) {
		std::cerr << "Я дятел, ";
	}
	

	return 0;
}
