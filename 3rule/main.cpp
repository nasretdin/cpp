#include <iostream>
#include <algorithm>
#include <cstring>



class String{
  public:
    String() = default;
    String(char c, int n): str(new char[n]), sz(n){
		memset(str, c, n);
		}
    ~String(){
      delete[] str;
    }
	
	String(const String& newstr){
		str = new char[newstr.sz + 1];
		memcpy(str, newstr.str, newstr.sz);
		sz = newstr.sz;
		}
    
    String& operator=(String newstr){
		std::swap(this->str, newstr.str);
        std::swap(this->sz, newstr.sz);
		return *this;
	}
    
  private:    
    char* str = nullptr;
    int sz = 0;
};

int main(){
  String str('q', 5);
  String str1 = str;
  str = str;
  //str1 = str;
}
