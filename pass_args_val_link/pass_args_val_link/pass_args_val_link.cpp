﻿#include <iostream>
#include <string>

using namespace std;

void print_result(int a, int *a_addr, int b, int *b_addr, string msg) {
  std::cout << msg << a_addr << ": " << a << " " << b_addr << ": " << b
            << std::endl;
}

void by_value(string msg) {
  msg = "!" + msg + "!";
  std::cout << msg << std::endl;
}

void by_link(string &msg) { std::cout << msg << std::endl; }

void swap_by_ref(int &a, int &b) {
  a = a + b;
  b = a - b;
  a = a - b;
  int *a_addr = &a;
  int *b_addr = &b;
  print_result(a, a_addr, b, b_addr, "In swap_by_ref func body: ");
}

void swap_by_val(int a, int b) {
  a = a + b;
  b = a - b;
  a = a - b;
  int *a_addr = &a;
  int *b_addr = &b;
  print_result(a, a_addr, b, b_addr, "In swap_by_val func body: ");
  // std::cout << "In func body: " << a << " " << b << std::endl;
}

int main() {
  string msg = "hello world";
  by_value(msg);
  by_link(msg);

  int a = 3;
  int b = 5;
  int *a_addr = &a;
  int *b_addr = &b;
  print_result(a, a_addr, b, b_addr, "Before by value: ");
  swap_by_val(a, b);
  print_result(a, a_addr, b, b_addr, "After by value and before by ref: ");
  swap_by_ref(a, b);
  print_result(a, a_addr, b, b_addr, "After by ref: ");
}
