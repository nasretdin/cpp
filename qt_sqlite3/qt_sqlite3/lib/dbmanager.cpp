#include "dbmanager.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QDebug>
#include <qlist.h>


DbManager::DbManager(const QString &path)
{
    m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db.setDatabaseName(path);

    if (!m_db.open())
    {
        qDebug() << "Error: connection with database fail";
    }
    else
    {
        qDebug() << "Database: connection ok";
    }
}

DbManager::~DbManager()
{
    if (m_db.isOpen())
    {
        m_db.close();
    }
}

bool DbManager::isOpen() const
{
    return m_db.isOpen();
}


bool DbManager::removePerson(const QString& name)
{
    bool success = false;

    if (personExists(name))
    {
        QSqlQuery queryDelete;
        queryDelete.prepare("DELETE FROM people WHERE name = (:name)");
        queryDelete.bindValue(":name", name);
        success = queryDelete.exec();

        if(!success)
        {
            qDebug() << "remove person failed: " << queryDelete.lastError();
        }
    }
    else
    {
        qDebug() << "remove person failed: person doesnt exist";
    }

    return success;
}


bool DbManager::personExists(const QString& name) const
{
    bool exists = false;

    QSqlQuery checkQuery;
    checkQuery.prepare("SELECT name FROM people WHERE name = (:name)");
    checkQuery.bindValue(":name", name);

    if (checkQuery.exec())
    {
        if (checkQuery.next())
        {
            exists = true;
        }
    }
    else
    {
        qDebug() << "person exists failed: " << checkQuery.lastError();
    }

    return exists;
}


bool DbManager::makeTable(DbObjBase& table)
{
    bool success = false;

    QSqlQuery query;
    QString tblnm = table.tableName();
    QList<Column> coumns = table.columnList();

    QString request = "CREATE TABLE "+ table.tableName() +"(id INTEGER PRIMARY KEY, ";

    for (int i = 0; i < table.columnList().count(); ++i){

        request += table.columnList().at(i).column_name + " " +
                table.columnList().at(i).column_type + " " + table.columnList().at(i).unique + " " +
                table.columnList().at(i).auto_increment + " " + table.columnList().at(i).not_null;
        if (i != table.columnList().count() - 1){
            request += ", ";
        }
    }

    request += ");";
    query.prepare(request);
    success = true;

    if (!query.exec())
    {
        qDebug() << "Couldn't create the table '" << table.tableName() << "Request: " << request;
        qDebug() << query.lastError().text();
        success = false;
    }

    return success;
}


bool DbManager::insert(DbObjBase& tableObject)
{
    bool success = false;

    if (tableObject.tableName() != ""){
        if(m_db.transaction()){
            QSqlQuery queryAdd(m_db);
            QString request = "INSERT INTO " + tableObject.tableName() + " ";
            QString values = "(";
            QString column_names = "(";

            for (int i=0; i < tableObject.Values().count(); ++i){
                values += "'" + tableObject.Values().at(i) + "'";
                column_names += tableObject.columnNames().at(i);
                if (i < tableObject.Values().count() - 1){
                    values += ", ";
                    column_names += ", ";
                } else{
                    values += ")";
                    column_names += ") ";
                }
            }
            request += column_names + " VALUES " + values + ";";
            queryAdd.prepare(request);
            success = true;
            if (!queryAdd.exec()){
                qDebug() << request << " Last error: " << queryAdd.lastError().text();
            }

            if(!m_db.commit()){
                qDebug() << "Failed to commit: " << m_db.lastError().text();
                m_db.rollback();
            }
        } else {
            qDebug() <<  "Failed to start transaction mode.";
            success = false;
        }

    } else {
        qDebug() << "Insert failed: tablename cannot be empty.";
    }
    return success;
}

void DbManager::selectOne(DbObjBase& tableObject)
{
    if (tableObject.tableName() != ""){
        QSqlQuery querySelect(m_db);
        QString colunmnames = "id, ";
        for (auto columnname : tableObject.columnNames()){
            if (columnname != tableObject.columnNames().last()){
            colunmnames += columnname + ", ";
            }else{
                colunmnames += columnname;
            }
        }

        QString request = "SELECT " + colunmnames + " FROM " + tableObject.tableName() + " WHERE id = " + tableObject.getObj_id() + ";";
        querySelect.prepare(request);
        if (!querySelect.exec()){
            qDebug() << request << " Last error: " << querySelect.lastError().text();
            return;
        }
        QList<QVariant> objectdata = {};

        while (querySelect.next()) {
            for (auto column: tableObject.columnList()){
                QString strValue = querySelect.value(column.column_name).toString();
                tableObject.setValue(column.column_name, strValue);
            }
        }


    }else{
        qDebug() << "Select failed: tablename cannot be empty.";

    }
}

//template<typename T>
//void DbManager<T>::_selectOne(T tableObject)
//{

//}

//template<typename T>
//void DbManager::selectOne(T& tableObject)
//{
//     if (tableObject.tableName() != ""){
//         QSqlQuery querySelect(m_db);
//         QString colunmnames = "id, ";
//         for (auto columnname : tableObject.columnNames()){
//             if (columnname != tableObject.columnNames().last()){
//             colunmnames += columnname + ", ";
//             }else{
//                 colunmnames += columnname;
//             }
//         }

//         QString request = "SELECT " + colunmnames + " FROM " + tableObject.tableName() + " WHERE id = " + tableObject.getObj_id() + ";";
//         querySelect.prepare(request);
//         if (!querySelect.exec()){
//             qDebug() << request << " Last error: " << querySelect.lastError().text();
//             return;
//         }
//         QList<QVariant> objectdata = {};

//         while (querySelect.next()) {
//             for (auto column: tableObject.columnNames()){
//                 auto value = querySelect.value(column);
//                 objectdata.append(value);
//             }
//         }

//         T& newobject(objectdata);
//         tableObject = newobject;

//     }else{
//         qDebug() << "Select failed";
//     }
//}

