#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QSqlDatabase>
#include "dbmodel.h"

/**
 * \class DbManager
 *
 * \brief SQL Database Manager class
 *
 * DbManager sets up the connection with SQL database
 * and performs some basics queries. The class requires
 * existing SQL database. You can create it with sqlite:
 * 1. $ sqlite3 people.db
 * 2. sqilte> CREATE TABLE people(ids integer primary key, name text);
 * 3. sqlite> .quit
 */

class DbManager
{
public:
    /**
     * @brief Constructor
     *
     * Constructor sets up connection with db and opens it
     * @param path - absolute path to db file
     */
    DbManager(const QString& path);

    /**
     * @brief Destructor
     *
     * Close the db connection
     */
    ~DbManager();

    bool isOpen() const;

    /**
     * @brief Remove person data from db
     * @param name - name of person to remove.
     * @return true - person removed successfully, false - person not removed
     */
    bool removePerson(const QString& name);

    /**
     * @brief Check if person of name "name" exists in db
     * @param name - name of person to check.
     * @return true - person exists, false - person does not exist
     */
    bool personExists(const QString& name) const;


    /**
     * @brief Create table in DB
     * @param table - instance of TableBase child
     * @return true - table added successfully, false - table not added
     */
    bool makeTable(DbObjBase& table);


    /**
     * @brief Insert new object in database table if it doesn't already exist
     * @return true - insert request if successfully, else - false
     */
    bool insert(DbObjBase& tableObject);


    /**
     * @brief Select TableBase child object from DB by id
     * @param tableObject - empty TableBase child reference with id value
     */
    void selectOne(DbObjBase& tableObject);



private:
    QSqlDatabase m_db;
};

#endif // DBMANAGER_H
