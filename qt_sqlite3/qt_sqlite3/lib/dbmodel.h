#ifndef DBMODEL_H
#define DBMODEL_H
#include <QString>
#include <QList>
#include <map>
#include <functional>
#include <QVariant>

enum DBTypes { INTEGER, TEXT, BLOB, REAL, NUMERIC };
static std::map<DBTypes, QString> FieldTypes = {{INTEGER, "INTEGER"},
                                                {TEXT, "TEXT"},
                                                {BLOB, "BLOB"},
                                                {REAL, "REAL"},
                                                {NUMERIC, "NUMERIC"}
                                               };
enum DBColumnAttributes {AUTOINCREMENT, NOT_NULL, UNIQUE, EMPTY};

static std::map<DBColumnAttributes, QString> ColumnAttributes = {{AUTOINCREMENT, "AUTOINCREMENT"},
                                                                 {UNIQUE, "UNIQUE"},
                                                                 {NOT_NULL, "NOT NULL"},
                                                                 {EMPTY, QString()}
                                                                };

struct Column{
    QString column_name;
    QString column_type;
    QString not_null = QString();
    QString auto_increment= QString();
    QString unique = QString();
    Column(QString columnname, DBTypes columntype,
           DBColumnAttributes notnull = EMPTY, DBColumnAttributes autoincrement = EMPTY, DBColumnAttributes unique = EMPTY);
};


class DbObjBase
{
private:
    const QString tablename;
    const QList<Column> columns;
    const QList<QString> column_names;
    const static quint16 obj_id = 0;


public:
    virtual QString tableName() = 0;
    virtual QList<Column> columnList() = 0;
    const virtual QList<QString> Values() = 0;
    const virtual QList<QString> columnNames() = 0;
    virtual const QString getObj_id() = 0;
    virtual void setValue(QString& columnname, QString& value) = 0;

};




class Person  final : virtual public DbObjBase {

    class MethodMapper{
    private:
        QString column_name;
        std::function<void(Person&, QString&)> func;
    public:
        MethodMapper(const QString& columnname, std::function<void(Person&, QString&)> funcname);

        std::function<void (Person&, QString&)> getFunc() const;
        QString getColumn_name() const;
    };

private:
    const quint16 obj_id;
    QString person_name;
    QString person_surename;
    const QList<QString> column_names = {"id", "name", "surename"};
    QList<MethodMapper> methodMapList = {{column_names.at(1), &Person::setPerson_name},
                                        {column_names.at(2), &Person::setPerson_surename}};


    // FIXME if type not INTEGER or NUMBER AUTOINCTEMENT can not be set!! (static_assert?)
    const QString tablename = "person";
    const QList<Column> columns {{column_names.at(1), TEXT, NOT_NULL, UNIQUE},
                                 {column_names.at(2), TEXT, NOT_NULL}};
    void setPerson_name(QString& value);
    void setPerson_surename( QString& value);

public:
    QString tableName();
    QList<Column> columnList();
    Person(QString personname, QString personsurename) ;
    Person(const quint16 objid) ;
    Person(QList<QVariant> valueslist);
    const QList<QString> Values();
    const QList<QString> columnNames();
    const QString getObj_id();
    void setValue(QString& columnname, QString& value);

};


#endif // DBMODEL_H
