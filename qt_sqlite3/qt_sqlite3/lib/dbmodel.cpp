#include "dbmodel.h"

#include <QString>

Person::Person(QString personname, QString personsurename):  obj_id(0), person_name(personname), person_surename(personsurename){}

Person::Person(const quint16 objid): obj_id(objid){}

Person::Person(QList<QVariant> valueslist) try:
    obj_id(valueslist.at(0).toInt()),
    person_name(valueslist.at(1).toString()),
    person_surename(valueslist.at(2).toString())
{

} catch(...) {

};


const QList<QString> Person::Values()
{
  return QList<QString>() << person_name << person_surename;
}

const QList<QString> Person::columnNames()
{
    return column_names;
}

const QString Person::getObj_id()
{
    return QString::number(this->obj_id);
}

void Person::setValue(QString& columnname, QString& value)
{

    for (auto methodnamepair: this->methodMapList){
        QString column_name = methodnamepair.getColumn_name();
        if (methodnamepair.getColumn_name() == columnname){
            std::function<void (Person&, QString&)> action = methodnamepair.getFunc();
            action(*this, value);
        }
    }
}


void Person::setPerson_surename(QString& value)
{
    person_surename = value;
}

void Person::setPerson_name(QString& value)
{
    person_name = value;
}

QString Person::tableName()
{
    return this->tablename;
}

QList<Column> Person::columnList()
{
    return this->columns;
}

Column::Column(QString columnname, DBTypes columntype,
               DBColumnAttributes notnull, DBColumnAttributes autoincrement, DBColumnAttributes unique) :
  column_name(columnname),
  column_type(FieldTypes[columntype]),
  not_null(ColumnAttributes[notnull]),
  auto_increment(ColumnAttributes[autoincrement]),
  unique(ColumnAttributes[unique]){}


Person::MethodMapper::MethodMapper(const QString& columnname, std::function<void (Person&, QString&)> funcname): column_name(columnname), func(funcname){}

std::function<void (Person&, QString&)> Person::MethodMapper::getFunc() const
{
    return func;
}

QString Person::MethodMapper::getColumn_name() const
{
    return column_name;
}
