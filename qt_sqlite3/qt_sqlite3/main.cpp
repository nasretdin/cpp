#include "mainwindow.h"

#include <QApplication>
#include "lib/dbmanager.h"


static const QString path = "example.db";

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  DbManager db(path);

  if (db.isOpen())
  {
//      db.createTable();   // Creates a table if it doens't exist. Otherwise, it will use existing table.
      Person person("Jim", "Beam");
      DbObjBase& personTbl = person;
      db.makeTable(personTbl);
      db.insert(person);
      Person out_person(1);
      db.selectOne(out_person);
      int i = 0;


}
  MainWindow w;
  w.show();
  return a.exec();
}
