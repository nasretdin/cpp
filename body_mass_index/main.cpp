//Упражнение 4
//Индекс массы тела (ИМТ) представляет массу человека в килограммах, деленную на квадрат роста в метрах
//(масса/(рост * рост)). Напишите программу, которая спрашивает у пользователя его вес (в киллограммах)
//и рост (сантиметрах), по ним вычисляет индекс массы тела и выводит его на консоль.

#include <iostream>

int main() {
  std::cout << "Enter your mass in kilograms: ";
  double mass = 0.0;
  std::cin >> mass;
  if (mass <= 0){
    std::cout << "Invalid value.\n";
    return 1;
  }

  std::cout << "Enter your height in centimeters: ";
  double height = 0;
  std::cin >> height;
  if (height <= 0){
    std::cout << "Invalid value.\n";
    return 1;
  }
  double height_m = height / 100.0;
  double bmi = mass / (height_m * height_m);

  std::cout << "Your body mass index is: " << bmi << std::endl;
  return 0;
}
