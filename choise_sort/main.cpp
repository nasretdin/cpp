#include <iostream>
#include <array>
#include <functional>

bool asc_expr(uint16_t one, uint16_t two) {
  return one > two;
}

bool desc_expr(uint16_t one, uint16_t two) {
  return one < two;
}

void SortArr(std::array<uint16_t, 10> &arr_num, const std::function<bool(uint16_t, uint16_t)> &direction) {
  for (int pos = 0; pos < arr_num.size(); ++pos) {
    for (int i = pos + 1; i < arr_num.size(); ++i) {
      if (!direction(arr_num.at(i), arr_num.at(pos))) {
        std::swap(arr_num.at(i), arr_num.at(pos));
      }
    }
  }
}

int main() {
  std::array<uint16_t, 10> arr_num = {22, 43, 6, 33, 9, 14, 26, 63, 3, 80};
  std::function<bool(uint16_t, uint16_t)> asc = asc_expr;
  std::function<bool(uint16_t, uint16_t)> desc = desc_expr;

  SortArr(arr_num, asc);
  for (auto num : arr_num) {
    std::cout << num << " ";
  }

  std::cout << '\n';

  SortArr(arr_num, desc);
  for (auto num : arr_num) {
    std::cout << num << " ";
  }

  return 0;
}
