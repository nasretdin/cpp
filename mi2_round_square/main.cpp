#include <iostream>
#include <cmath>
int main() {
  const double pi = 3.141592;
  std::cout << "Enter radius:";

  uint16_t radius = 0;
  std::cin >> radius;
  double square = pi * radius * radius;

  std::cout << "The square of round is: " << square << std::endl;
  return 0;
}
