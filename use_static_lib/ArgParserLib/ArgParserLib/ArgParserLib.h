#pragma once
#include <list>

struct Argument {
  std::string key;
  std::string name;
  bool required;
};

class CmdLineArgParser {
public:
  CmdLineArgParser();

  void AddArgument(string key, string name, string help, bool required);
  std::list<Argument> Parse(int argc, char *argv[]);
  void PrintHelp();
};

class ArgError {
protected:
  int error_number;          
  std::string error_message; 

public:
  explicit ArgError(const std::string &msg, int err_num)
      : error_number(err_num), error_message(msg) {}
  ~ArgError() throw() {}

  const char *what() const throw() { return error_message.c_str(); }
};