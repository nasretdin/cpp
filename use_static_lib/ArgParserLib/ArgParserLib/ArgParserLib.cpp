﻿// ArgParserLib.cpp : Определяет функции для статической библиотеки.
//

#include "ArgParserLib.h"
#include "pch.h"
#include <iostream>
#include <list>

using namespace std;

list<Argument> arguments;

CmdLineArgParser::CmdLineArgParser() {}

void CmdLineArgParser::AddArgument(string key, string name, string help,
                                   bool required) {
  Argument argument;
  argument.key = key;
  argument.name = name;
  argument.required = required;

  auto arg_list_size = arguments.begin();
  arguments.insert(arg_list_size, argument);
}

std::list<Argument> CmdLineArgParser::Parse(int argc, char *argv[]) {
  for (int count = 0; count < argc; count++)
    cout << "  argv[" << count << "]   " << argv[count] << "\n";
  return arguments;
}

void PrintHelp() {}
